﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Estudos.Model;
namespace Estudos.Context
{
    public class EstudosContext : DbContext
    {

        public EstudosContext() : base(ObterStringConnection())
        {   
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AulasMap());
        }

        private static string ObterStringConnection()
        {
            var sqlConnection = new SqlConnectionStringBuilder();
            sqlConnection.InitialCatalog = "ESTUDOS";
            sqlConnection.DataSource = "ENGE056";
            sqlConnection.UserID = "Engegraph";
            sqlConnection.Password = "DevEngegraph";
            return sqlConnection.ConnectionString;
        }
        public DbSet<Aulas> Aulas { get; set; }
    }

    public class AulasMap : EntityTypeConfiguration<Aulas>
    {
        public AulasMap()
        {
            ToTable("Aulas");

        }
    }
}
