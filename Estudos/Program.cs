﻿using Estudos.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudos
{
    class Program
    {
        public static void Main(string[] args)
        {
            var aula = new EstudosContext();

            aula.Aulas.Add(new Model.Aulas() 
            {
                Id =1,
                Nome = "Brendon."
            });

            aula.SaveChanges();

            Console.ReadKey();
            
        }
    }
}
    