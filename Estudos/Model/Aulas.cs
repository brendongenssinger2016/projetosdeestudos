﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudos.Model
{
    public class Aulas
    {
        [Key]
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
