﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aula_6_ReflectionAndGenerics.ClassesAulas;
namespace Aula_6_ReflectionAndGenerics
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(typeof(Aula_1).Name + "\n");
            //Aula 1

            Aula_1._Aula1();
            Console.WriteLine("******************************************************");
            Console.WriteLine();

            // Aula 2
            Aula_2._Aula2();
            Console.WriteLine("******************************************************");
            Console.Read();
        }
    }

    public class Employee
    {
        public string Nome { get; set; }

        public void Speak<T>()
        {
            Console.WriteLine(typeof(T).Name);
        }
    }

    public class ExecutarOMetodo
    {
        public void teste()
        {
            Console.WriteLine("Mostrando o metodo Teste da classe ExecutarOMetodo");
        }
    }
}
