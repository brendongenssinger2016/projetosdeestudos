﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aula_6_ReflectionAndGenerics.ClassesAulas
{
    public class Aula_2
    {
        //
        // Resumo:
        //     Invokes the method or constructor represented by the current instance, using
        //     the specified parameters.
        //
        public static void _Aula2()
        {
            // Nesta opção eu utilizo o metodo da classe
            var employee = new ExecutarOMetodo();
            var employeeType = typeof(ExecutarOMetodo);
            var methodInfo = employeeType.GetMethod("teste");            
            methodInfo = methodInfo.MakeGenericMethod(typeof(DateTime)); // Caso o metodo seja generico.           
            methodInfo.Invoke(employee, null); // Invoke já chama o metodo.
            employee.teste(); // já chama o metodo;
            
        }

    }
}
