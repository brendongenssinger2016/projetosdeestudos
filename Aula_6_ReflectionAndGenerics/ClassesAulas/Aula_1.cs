﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aula_6_ReflectionAndGenerics.ClassesAulas
{
    public static class Aula_1
    {
        public static void _Aula1()
        {
            var employeeList = CreateList(typeof(List<>), typeof(List<Employee>));
            Console.Write(employeeList.GetType().Name);
            Console.WriteLine(employeeList.GetType().Name); // Nome do tipo da classe.
            var genericArguments = employeeList.GetType().GenericTypeArguments;
            foreach (var generics in genericArguments)
            {
                Console.Write("[{0}]", generics.Name); // Nome do tipo da classe.   
            }
        }

        private static object CreateList(Type collectionType, Type itemType)
        {
            var closedType = collectionType.MakeGenericType(itemType);
            return Activator.CreateInstance(closedType);
        }
    }
}
