﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoCaliburnMicro.Model
{
    public class Variaveis
    {
        public string NomeCampo { get; set; }
        public string DataSet { get; set; }
        public string DataField { get; set; }
        public string NovoDataSet { get; set; }
        public string NovoDataField { get; set; }
        public List<string> ListDataSet { get; set; }

    }
}
