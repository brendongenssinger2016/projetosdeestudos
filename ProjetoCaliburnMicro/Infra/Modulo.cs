﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace ProjetoCaliburnMicro.Infra
{
    public class Modulo : Screen
    {
        public Modulo()
        {
            PropertyChanged += Modulo_PropertyChanged;
        }

        public virtual void Modulo_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
        }

        public virtual void Fechar()
        {
            this.TryClose(true);
        }

        
    }
}
