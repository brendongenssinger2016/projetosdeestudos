﻿using ProjetoCaliburnMicro.Infra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ProjetoCaliburnMicro.ViewModels
{
    public class UsoAsyncViewModel : Modulo
    {
        private string _messageAsync;
        private bool _tarefaStart = false;
        public string MensagemAsync
        {
            get => _messageAsync;
            set
            {
                _messageAsync = value;
                NotifyOfPropertyChange(() => MensagemAsync);
            }
        }
        public UsoAsyncViewModel()
        {
        }

        public void TestarAsync()
        {
            MessageBox.Show("Button Clicked !");
            ExecuteAsync();
        }

        private async void ExecuteAsync()
        {
            if(!_tarefaStart)
            {
                _tarefaStart = true;

                await Task.Run(() => 
                {
                    while (true)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            MensagemAsync = $"Async {i}";                    
                        }
                    }
                });

            }
        }
    }
}
