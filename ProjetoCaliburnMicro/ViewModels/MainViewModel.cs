﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using ProjetoCaliburnMicro.Infra;
using ProjetoCaliburnMicro.Views;

namespace ProjetoCaliburnMicro.ViewModels
{
    public class MainViewModel : Conductor<Modulo>.Collection.OneActive
    {
        private string _firstName;
        private string _concateName;
     
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
                NotifyOfPropertyChange(() => FirstName);
            }
        }
        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
            }
        }

        public string Result
        {
            get
            {
                
                    Thread.Sleep(200);
                return $"{_firstName} {_lastName}";
                
            }
           
        }

        private string _endereco;
        public string Endereco
        {
            get { return _endereco; }
            set { _endereco = value;
                NotifyOfPropertyChange(() => Endereco);
            }
        }

        public void ChamarLista()
        {
            Modulo item;
            item = new UsoListComboBoxViewModel() as Modulo;
           ActivateItem(item);
        }

        public void ModeTwoWay()
        {
            Modulo item;
            item = new UsoTwoWayViewModel() as Modulo;
            ActivateItem(item);
        }

        public void UsoAsync()
        {
            Modulo item = new UsoAsyncViewModel();
            ActivateItem(item);
        }

        public void UsoTabControl()
        {
            var item = new UsoTabControlViewModel();
            ActivateItem(item);
        }
    }
}
