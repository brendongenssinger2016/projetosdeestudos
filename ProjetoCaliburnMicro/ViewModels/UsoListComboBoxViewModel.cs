﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using ProjetoCaliburnMicro.Infra;
using ProjetoCaliburnMicro.Model;

namespace ProjetoCaliburnMicro.ViewModels
{
    public class UsoListComboBoxViewModel: Modulo
    {
        private ObservableCollection<Variaveis> _listVariaveis;
        private Variaveis _variaveisSelecionada;
        private ObservableCollection<string> _listDataSet;
        private string _valor;

        public UsoListComboBoxViewModel()
        {
            ListVariaveis = new ObservableCollection<Variaveis>();
            
            ListVariaveis.Add(
                new Variaveis(){DataField= "Data Field 1",DataSet = "Data Set 1",NovoDataField="Novo Data Field 1", NovoDataSet="Novo DataSet 1", ListDataSet  =  new List<string>() { "Novo DataSet 1" } });

            ListVariaveis.Add(new Variaveis() { DataField = "Data Field 2", DataSet = "Data Set 3", NovoDataField = "Novo Data Field 4", NovoDataSet = "Novo DataSet 5", ListDataSet= new List<string>() { "Novo DataSet 1" } });

            NotifyOfPropertyChange(() => ListVariaveis);            

        }
        public ObservableCollection<Variaveis> ListVariaveis
        {
            get => _listVariaveis;
            set
            {
                _listVariaveis = value;
                NotifyOfPropertyChange(() => ListVariaveis);
            }
        }
        public string Valor
        {
            get => _valor;
            set
            {
                _valor = value;
                NotifyOfPropertyChange(() => Valor);
            }
        }

        public Variaveis VariaveisSelecionada
        {
            get => _variaveisSelecionada;
            set
            {
                _variaveisSelecionada = value;
            }
        }

        public void Teste(object sender, TextCompositionEventArgs e)
        {
            MessageBox.Show(Valor);
        }

        public override void Modulo_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //base.Modulo_PropertyChanged(sender, e);
        }

        public override void Fechar()
        {
            this.TryClose();
        }
    }
}
