﻿using ProjetoCaliburnMicro.Infra;
using ProjetoCaliburnMicro.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ProjetoCaliburnMicro.ViewModels
{
    public class UsoTabControlViewModel : Modulo
    {
        private ObservableCollection<TabControle> _listaTabControl;

        public ObservableCollection<TabControle> ListaTabControl
        {
            get => _listaTabControl;
            set
            {
                if (value is null || value== _listaTabControl) return;                
                _listaTabControl = value;
                NotifyOfPropertyChange(() => ListaTabControl);
            }
        }

        public UsoTabControlViewModel()
        {
            
        }

        public void CarregarLista()
        {
            ListaTabControl = new ObservableCollection<TabControle>();
            ListaTabControl.Add(new TabControle() { NomeTab = "1" });
            ListaTabControl.Add(new TabControle() { NomeTab = "2" });
            ListaTabControl.Add(new TabControle() { NomeTab = "3" });
            ListaTabControl.Add(new TabControle() { NomeTab = "4" });
            NotifyOfPropertyChange(() => ListaTabControl);
        }
    }
}
