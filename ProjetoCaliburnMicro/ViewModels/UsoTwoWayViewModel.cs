﻿using ProjetoCaliburnMicro.Infra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoCaliburnMicro.ViewModels
{
    public class UsoTwoWayViewModel : Modulo
    {
        private string _nome;
        private string _sobreNome;

        public UsoTwoWayViewModel()
        {
            Nome = "Brendon";
            SobreNome = "Mascarenhas";
        }



        public string Nome
        {
            get => _nome;
            set
            {
                _nome = value;
                NotifyOfPropertyChange(() => Nome);
            }
        }

        public string SobreNome
        {
            get => _sobreNome;
            set
            {
                _sobreNome = value;
                NotifyOfPropertyChange(() => SobreNome);
            }
        }
    }
}
