﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using EvalServiceLibrary.Model;
using EvalServiceLibrary.Service;
using EvalServiceLibrary.Interfaces;
namespace ConsoleHost
{
    public class ConsoleHostEvalWCF
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Servidor **");
            var host = new ServiceHost(typeof(EvalService));
            
            host.AddServiceEndpoint(typeof(IEvalService),
                new BasicHttpBinding(),
                "http://localhost:8733/evals/basic");

            host.AddServiceEndpoint(typeof(IEvalService),
                new WSHttpBinding(),
                "http://localhost:8733/evals/ws");

            host.AddServiceEndpoint(typeof(IEvalService),
                new NetTcpBinding(),
                "net.tcp://localhost:5212/Design_Time_Addresses/evals");            
            try
            {
                host.Open();
                PrintServiceInfo(host);
                Console.Read();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Read();
            }            
        }

        private static void PrintServiceInfo(ServiceHost host)
        {
            Console.WriteLine($@"Is up and runing with these endpois: {host.Description.ServiceType}");
            foreach (var descriptionEndpoint in host.Description.Endpoints)
            {
                Console.WriteLine(descriptionEndpoint.Address);
            }
        }
    }
}
