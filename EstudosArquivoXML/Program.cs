﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace EstudosArquivoXML
{
    public class Program
    {
        static void Main(string[] args)
        {
            GerarVariosTiposDeXML(); 
        }

        private static void GerarVariosTiposDeXML()
        {
            GerarXMlManual();
            GerarXmlComModel();

        }

        private static void GerarXmlComModel()
        {
            var model = new ModelXML();            
            model.nome = "Brendon";
            var teste1 = new TesteXml();
            teste1.nome1 = "1";
            model.testexml = teste1;
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(model.GetType());            
            x.Serialize(new StreamWriter("xmlModel.xml"), model);
            var result = x.Deserialize(new StreamReader("xmlModel.xml")) as TesteXml;
        }

        private static void GerarXMlManual()
        {
            var xmlnew = new XDocument(new XDeclaration("1.0", "utf-8", "standalone"));
            xmlnew.Add(new XElement("Element1"));
            xmlnew.Element("Element1").Add(new XAttribute("Attritute1", ""), new XAttribute("Attribute2", ""));
            xmlnew.Element("Element1").Add(new XElement("ElementSozim1"));
            xmlnew.Element("Element1").Add(new XElement("Element2SemAttribute"));
            xmlnew.Element("Element1").Add(new XElement("Element3SemAttribute"));
            xmlnew.Element("Element1").Element("Element3SemAttribute").Add(new XAttribute("Attribute1", ""));
            xmlnew.Save("xmlManualSemModel.xml");

            var xmlTeste = new XmlDocument();
            xmlTeste.LoadXml("caminhoDoXML");
            var resul = xmlTeste.SelectNodes("/Elemento1/Elemento2/Elemento3");


            xmlnew.Elements().FirstOrDefault(pX => pX.Name == "").Attributes().FirstOrDefault(pX => pX.Name=="");
            
            xmlnew.Elements().FirstOrDefault(pX => pX.Name == "").
                Attributes().FirstOrDefault(pX => pX.Name == "").SetValue("teste");

            xmlnew.Elements().FirstOrDefault(pX => pX.Name == "").SetValue("inserir elemento");

        }
    }
    
    public class ModelXML
    {
        [XmlElement(ElementName = "ModelXMlProp")]        
        public string nome { get; set; }
        [XmlElement(ElementName = "ModelXMlProp22")]
        public TesteXml testexml { get; set; }

    }

    public class TesteXml
    {
        [XmlAttribute(AttributeName = "Teste22")]
        public string nome1 { get; set; }
    }
}
