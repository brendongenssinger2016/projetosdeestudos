﻿using System;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ValidacaoXSD
{
    public class Program
    {        
        public static string nomeXML = "teste.xml";
        public static void Main(string[] args)
        {
            try
            {
                string strXsdFilePath = @"v1_22-geral.xsd";
                
                var objeto = CriarObjeto();
                var dados = ObterDados(objeto);
                CriarOXml(dados);


                XmlReaderSettings readerSettings = new XmlReaderSettings();
                readerSettings.ValidationType = ValidationType.Schema;
                XmlSchemaSet schemaset = new XmlSchemaSet();
                readerSettings.Schemas = schemaset;                
                schemaset.Add(null, strXsdFilePath);

                readerSettings.ValidationEventHandler += ValidationEventHandler;

                XmlReader xmlReader = XmlReader.Create(nomeXML, readerSettings);

                try
                {
                    // faz leitura de todos os dados do xml.
                    while (xmlReader.Read()) { }
                    {

                    }
                }
                catch (Exception eX)
                {
                    // um erro ocorre se o documento xml inclui caracteres ilegais ou tags que não estão ainhadas corretamente.
                    Console.WriteLine("Erro ao validar xml " + eX.Message);
                    
                }
                
            }
            catch (Exception eX)
            {
                Console.WriteLine(eX.Message);
                Console.ReadLine();
            }

            Console.ReadLine();
        }
        static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    Console.WriteLine("Error: {0}", e.Message);
                    break;
                case XmlSeverityType.Warning:
                    Console.WriteLine("Warning {0}", e.Message);
                    break;
            }
        }

        private static void CriarOXml(estadosEstado dados)
        {
            XmlWriterSettings setting = new XmlWriterSettings();
            setting.Indent = true;
            setting.Encoding = Encoding.UTF8;            
            
            var xmlWriter = XmlWriter.Create(nomeXML,setting);
            var serializer = new XmlSerializer(dados.GetType());
            serializer.Serialize(xmlWriter, dados);
            xmlWriter.Flush();
            xmlWriter.Dispose();
        }

        private static estadosEstado ObterDados(estadosEstado objeto)
        {
            objeto.cartorios = new estadosEstadoCartorios();
            objeto.uf = enderecoUF.GO;
            objeto.cartorios.total = 0;
            
            objeto.cartorios.cartorio = new estadosEstadoCartoriosCartorio[]
           {
                new estadosEstadoCartoriosCartorio()
                {
                    apresentantes = new estadosEstadoCartoriosCartorioApresentantes()
                    {
                        apresentante = new estadosEstadoCartoriosCartorioApresentantesApresentante[]
                        {
                            new estadosEstadoCartoriosCartorioApresentantesApresentante()
                            {
                                codigo = "1",
                                enderecos = new enderecos(),
                                Item = "item",
                                ItemElementName = ItemChoiceType.CPF,
                                nome = "Teste engegraph",
                                tipo_apresentante = 1,
                                titulos = new apresentanteTitulos()
                                {
                                    total = 0,

                                    titulo = new apresentanteTitulosTitulo[]
                                    {
                                        new apresentanteTitulosTitulo()
                                        {
                                            aceite = tituloAceite.A,
                                            aceiteSpecified = true,
                                            acervo_cartorio =0,
                                            acervo_cartorioSpecified = true,
                                            acervo_herdado = true
                                        },

                                        new apresentanteTitulosTitulo()
                                        {
                                            sacador = new sacador()
                                            {

                                            },
                                            cedente = new cedente()
                                            {

                                            }

                                        }
                                        
                                    },
                                    
                                    
                                }
                            }
                        },
                        total = 0                       
                    },
                    codigo = "111",
                    comarca = 0,

                },
                new estadosEstadoCartoriosCartorio(){},
                new estadosEstadoCartoriosCartorio(){},
                new estadosEstadoCartoriosCartorio(){},
           };

            return objeto;
        }

        private static estadosEstado CriarObjeto()
        {
            return new estadosEstado();
        }

       

       
    }
}
