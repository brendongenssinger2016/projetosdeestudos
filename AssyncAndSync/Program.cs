﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssyncAndSync
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite a sua mensagem");
            var msg = Console.ReadLine();
            ISampleService sample = new SampleService();
            Console.Write(sample.SampleMethodTaskAsync(msg).Result.ToString());
            Console.Read();
        }
    }

   public interface ISampleService
    {
       Task<string> SampleMethodTaskAsync(string msg);
    }

    class SampleService : ISampleService
    {
        public Task<string> SampleMethodTaskAsync(string msg)
        {
            return Task<string>.Factory.StartNew(()=>
            {
                return msg;
            });

        }
    }
}
