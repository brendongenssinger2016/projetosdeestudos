﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcursoSaneago
{
    class Program
    {
        private static int qtdPercorrida = 0, qtdPercorreuZero=0;
        static void Main(string[] args)
        {

            /*
             *
             * Estudo sobre uso de recursiva, ele percorre 4 vezes.
             * 4*4 = 16 são duas funções, se torna 32;
             *
             */
            Console.WriteLine("Usando a função...");
            var valorRetornado = f(5);
            Console.WriteLine($"Valor Retornado => {valorRetornado} / {qtdPercorrida} / {qtdPercorreuZero} " );

            Console.ReadLine();
        }

        public static int f(int n)
        {
            qtdPercorrida++;
            if (n == 0)
            {
                qtdPercorreuZero++;
                return 1;
            }
                
            else
                --n;

            return f(n) + f(n);
        }
    }

    enum XQualFunction
    {
        func1,func2,nenhum
    }
}
