﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DicasEArmadilhas
{
    public class Part3
    {
    }

    [Obsolete]
    public class UseThisClass
    {
        
        public void Mensage() {
            Console.WriteLine("Mensagem");            
        }
    }

    [Obsolete("USE OTHER CLASS NAME TESTE")]
    public class Teste
    {
        public void Mensage()
        {
            Console.WriteLine("Mensagem");
        }
    }

    [Obsolete("",true)]
    public class MensagemDeErro { }
}
