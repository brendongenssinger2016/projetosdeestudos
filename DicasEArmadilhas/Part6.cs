﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DicasEArmadilhas
{
    public class Part6
    {
        public static Part6 instancia = new Part6();

        public void UsingConditionalDirectives()
        {
            var logger = new FakeLogger();

            logger.WriteLogMessage("Message 1");
            logger.WriteLogMessage("Message 2");

        }

        #region Video4

        public void UsingClassPathIO()
        {
            var path = @"C:\temp\pspathdemo\test.txt";
            path = Path.ChangeExtension(path, "bak");
            var dirName = Path.GetDirectoryName(path);
            var ext = Path.GetExtension(path);
            var file = Path.GetFileName(path);
            var fileNoExt = Path.GetFileNameWithoutExtension(path);
            bool hasExt = Path.HasExtension(path);
        }

        public void UseFulGeneralMethods()
        {
            var invalidNameChars = Path.GetInvalidFileNameChars();
            var rndFileName = Path.GetRandomFileName();
            var rndTempFile = Path.GetTempFileName();
            var userTempPath = Path.GetTempPath();
            char platformSpecificDirSeparater = Path.DirectorySeparatorChar;
        }

        #endregion

        #region Video5

        public void TheEasierWay()
        {
            var name = "Claire";
            var age = 30;
            var gender = "female";

            var output = string.Format("Name:{0,-20} Age:{1,-10} Gender:{2,0}", name, age, gender);
            Debug.WriteLine(output);
        }

        #endregion

        #region Video6

        private void WriteDebug(string message, params object[] objects)
        {
            Debug.WriteLine(message);
            foreach (var o in objects)
            {
                Debug.WriteLine(o);
            }
        }

        public void WriteDebugExample()
        {
            WriteDebug("Different objects", 42, new Uri("http://pluralsight"), 2, 3, 4, 5, 6);
        }

        #endregion

        #region Video8
        public void SystemRelatedInfo()
        {
            var envVars = Environment.GetEnvironmentVariables();
            var envPathVar = envVars["Path"];

            var is64BitOs = Environment.Is64BitOperatingSystem;
            var is64BitProc = Environment.Is64BitProcess;
            var numberOfProcs = Environment.ProcessorCount;
            var pageSize = Environment.SystemPageSize;
            var clrRuntimeVersion = Environment.Version;
            var currEnvNewLineString = Environment.NewLine;
        }

        public void DirectoryRelatedInfo()
        {
            var currentWorkingDir = Environment.CurrentDirectory;
            var desktopDir = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var fontsDir = Environment.GetFolderPath(Environment.SpecialFolder.Fonts);
            var myDocsDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var usersStartupDir = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
            var drivers = Environment.GetLogicalDrives();
            var sysDir = Environment.SystemDirectory;
        }
        #endregion

        #region Video9

        public void ExecutarIndexacao()
        {
            var ind = new Indexador(); // Crio o objeto.
            var valorDoInd = ind[20]; //Get valor do index 20
            valorDoInd++; // Incremento mais um
            ind[20] = valorDoInd; // Set valor do index 20
        }

        #endregion

    }

    class FakeLogger
    {
        [Conditional("LOG_")]
        public void WriteLogMessage(string message)
        {
            Debug.WriteLine("DEBUG MESSAGE: " + message);
        }
    }

    #region Video7

    public class Person
    {
        private readonly string Name;
        private readonly int Age;
        private readonly string _gender;
        private const string DefaultGender = "default";

        public Person(string name) : this(name, int.MinValue, DefaultGender)
        {

        }

        public Person(string name, int age, string gender)
        {
            Name = name;
            Age = age;
            _gender = gender;
        }

        public Person(string name, int age)
        {
        }


        #endregion


    }

    #region 9

    public class Indexador
    {
        private int[] _indexador;

        public int this[int index]
        {
            get => _indexador[index];
            set => _indexador[index] = value;
        }
    }

    #endregion
}
