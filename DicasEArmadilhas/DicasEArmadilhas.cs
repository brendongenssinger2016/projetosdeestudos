﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;

namespace DicasEArmadilhas
{
    public class DicasEArmadilhas
    {
        public static void _Date()
        {
            var minDate = DateTime.MinValue;
            var dateLocal = minDate.ToLocalTime();
            Console.WriteLine($"DateTime.MinValue {minDate} - dateTolocalTime {dateLocal}");
            var _bool = minDate == dateLocal;
            Console.ReadKey();

        }

        public static void UnicodeValidityExample()
        {
            var validChar = 'A';
            var ucCategory = char.GetUnicodeCategory(validChar);
            var isValidUnico = ucCategory != UnicodeCategory.OtherNotAssigned;
            Console.WriteLine(isValidUnico);
            Console.ReadKey();

            var invalidCharacter = (char) 888;
            ucCategory = char.GetUnicodeCategory(invalidCharacter);
            isValidUnico = ucCategory != UnicodeCategory.OtherNotAssigned;
            Console.Read();
        }

        public static void ChangingThreadCultureExample()
        {
            var turkeyCultureString = "tr-TR";
            Console.WriteLine("Setting Turkish Language - Turkey country/Region");
            var turkeyCultureInfo = CultureInfo.GetCultureInfo(turkeyCultureString);
            Thread.CurrentThread.CurrentCulture = turkeyCultureInfo;
            var textEmTurco = "i".ToUpper();
            Console.WriteLine(textEmTurco);
            Console.ReadKey();
        }

        public static void UsingTuplesESortedDictionary()
        {
            var t1 = Tuple.Create(1, "a");
            var t2 = Tuple.Create(1, "b");
            var t3 = Tuple.Create(1, "aa");

            var d = new SortedDictionary<Tuple<int,string>,string>();
            d.Add(t1,"Tupla t1");
            d.Add(t2,"tupla t2");
            d.Add(t3,"Tupla t3");
            foreach (var item in d)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }
        
    }
}