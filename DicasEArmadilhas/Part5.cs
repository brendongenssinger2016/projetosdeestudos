﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace DicasEArmadilhas
{
    #region Aula5
    public class Part5
    {
        /**
         *  Uso do Yield, ele retorna um Enumerable.
         *  Ele é um interator, quando o metodo executa novamente, ele já sabe onde parou.
         *  Para analisar, observar na saida do console.
         * 
         */


        public IEnumerable<int> UsingYieldReturn(IEnumerable<int> numbers)
        {
            foreach (var number in numbers)
            {
                Debug.WriteLine("Using Yield: " + number);
                if (number % 2 != 0) continue;
                Debug.WriteLine("yielding:" + number);
                yield return number;
            }
        }


        public void UsingBigInteger()
        {
            BigInteger b1 = 10000;
            BigInteger b2 = 10000;
            var result = b1 * b2 + 45 / 2;
            result = result % 5;
            result = BigInteger.Negate(result);
            result = BigInteger.Max(b1, b2);
        }

        public static void NaughtyWordCensor()
        {
            const string naughtyVersion = "He is a poopy head and darn idiot!";
            /**string.format aceita objeos que herdam do IFormatProvider
             
             */
            //var cleanVersion = string.Format(new Aula5_Part2(),
            //                    "Clean Verson:{0}", naughtyVersion);

            var cleanVersion = string.Format(new Aula5_Part2(), "Clean Version: {0:xxx},{1}", naughtyVersion,
                "Some other darn string.");

            Debug.WriteLine(cleanVersion);
        }
    } 
    #endregion

    #region Aula5_part2

    public class Aula5_Part2 : IFormatProvider, ICustomFormatter
    {
        private readonly Dictionary<string, string> _dictionary = null;

        public Aula5_Part2()
        {

            _dictionary = new Dictionary<string, string>
            {
                {"oi","o*"},{"mano","ma**"},{"blabla","bl***"},{"poopy","p**y"},
                {"Darn","D*rn"}
            };
        }
        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
            {
                return this;
            }
            return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if(format=="xxx")
                return CensorNaughtyWords(arg.ToString());
            else
            return arg.ToString();
        }

        private string CensorNaughtyWords(string s)
        {   
            return s.Replace("poopy", "p**y").Replace("Darn", "d*rn");
        }
    }
    #endregion


}
