﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DicasEArmadilhas
{
    public class ChamarAClasse
    {
        public static void Chamar()
        {
            const string teste = "Test";
            var _teste =  string.Format(new ClasseDeTeste(),"Teste:{0}", teste);
        }
    }

    public class ClasseDeTeste : ITeste,ITeste2, IFormatProvider
    {
        public ClasseDeTeste()
        {
            
        }
        public object Gravar(string s)
        {
            throw new NotImplementedException();
        }

        public string Comparar(string s)
        {
            throw new NotImplementedException();
        }

        public object GetFormat(Type formatType)
        {
            throw new NotImplementedException();
        }
    }

    public interface ITeste
    {
        object Gravar(string s);
    }

    public interface ITeste2
    {
        string Comparar(string s);
    }
}
