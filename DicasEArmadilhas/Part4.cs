﻿/* Adiciona a referencia, clica com o botão direito em cima da referencia 
 * clica em propriedades e no aliases altera pra um nome que você mesmo identifica
 * em seguida acrescenta   < extern alias > e o nome que colocou no aliases;
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace DicasEArmadilhas
{
    /*
     * Enumerador NumberStyles pode ser usado quando estamos analisando valores numéricos.
     * 
     */ 
    [TestClass]
    public class Part4
    {
        [TestMethod]
        public void Aula2()
        {
            var f1 = float.Parse("2300", NumberStyles.Number);

            var nfi = new NumberFormatInfo
            {
                CurrencyDecimalDigits =4
            };
            var f = float. Parse("240,00", nfi);
        }

        [TestMethod]
        public void AmbiguousParse()
        {
            const string ambiguousDateString = "01/12/2000";
            var d = DateTime.Parse(ambiguousDateString);
            Debug.WriteLine(d.ToLongDateString());

            d = DateTime.ParseExact(ambiguousDateString, "MM/dd/yyyy", null);
            Debug.WriteLine(d.ToLongDateString());
        }

        [TestMethod]
        public void DateTimeOffSetExample()
        {
            var original = new DateTimeOffset(2000,12,1,13,30,0,TimeSpan.FromHours(-8));
            var safeRoundTripString = original.ToString("O");
            var reparsed = DateTimeOffset.Parse(safeRoundTripString);

            Debug.WriteLine(reparsed);
            Debug.WriteLine(safeRoundTripString+"Format ISO");
        }

        [TestMethod]
        public void UseDateTime()
        {
            var d1 = DateTime.Parse("01/12/2000", null, DateTimeStyles.AssumeUniversal);
            var d2 = DateTime.Parse("01/12/2000", null, DateTimeStyles.AssumeLocal);
            var d3 = DateTime.Parse("01/12/2000", null, DateTimeStyles.NoCurrentDateDefault);
            var d4 = DateTime.Parse("01/12/2000", null, DateTimeStyles.None);
            Debug.WriteLine(d1 +
                            "\n" + d2 +
                            "\n" + d3 +
                            "\n" + d4 +
                            "\n");
        }
    }
}
