﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Estudo.Reflexao.Negocios;
namespace Estudo.Reflexao
{
    public static class Inicio
    {
        public static void Main(string[] args)
        {
            new XNegocio_1().ExecutarReflexao();
            new XNegocio_2().ExecutarReflexaWithAssembly();
        }
    }
}
