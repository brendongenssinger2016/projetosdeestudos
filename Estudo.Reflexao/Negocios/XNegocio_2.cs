﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Components;
using Ninject;
using Ninject.Modules;
namespace Estudo.Reflexao.Negocios
{
    public class XNegocio_2
    {
        public void ExecutarReflexaWithAssembly()
        {
            try
            {
                XKernel.Instancia = new StandardKernel(new XInfraEstrutura());
                Console.WriteLine("Deu certo");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

    public class XInfraEstrutura : NinjectModule
    {
        public override void Load()
        {
            var interfaceIniciavel = typeof(IInicializavel);
            // Busca todas classes que herdam da interface IInicializavel
            var tipos = typeof(XInfraEstrutura).Assembly.GetTypes().Where(px => interfaceIniciavel.IsAssignableFrom(px) && !px.IsInterface);            
            foreach (var item in tipos)
            {
                //Percorre por cada Type e criam o bind.
                Bind(interfaceIniciavel).To(item).InTransientScope();
            }
        }
    }
    public interface IInicializavel
    {
        void Inicializar();
        void Prioridade();
    }
    public class Teste : IInicializavel
    {
        public void Inicializar()
        {
            throw new NotImplementedException();
        }

        public void Prioridade()
        {
            throw new NotImplementedException();
        }
    }

    public class Teste1 : IInicializavel
    {
        public void Inicializar()
        {
            throw new NotImplementedException();
        }

        public void Prioridade()
        {
            throw new NotImplementedException();
        }
    }

    public class Teste2 : IInicializavel
    {
        public void Inicializar()
        {
            throw new NotImplementedException();
        }

        public void Prioridade()
        {
            throw new NotImplementedException();
        }
    }
}
