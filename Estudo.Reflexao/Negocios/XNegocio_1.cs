﻿using Estudo.Reflexao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Reflection.Emit;

namespace Estudo.Reflexao.Negocios
{
    public class XNegocio_1
    {
        internal void ExecutarReflexao()
        {
            Execute();
        }

        private void Execute()
        {
            var listaItemB = new List<ItemB>();
            listaItemB.Add(new ItemB()
            {
                PropID=1,
                PropDesc="Tx_1"
            });
            ExecutarListaItemA(listaItemB.ToArray());
          
        }

        private void ExecutarListaItemA(ItemB[] itemB)
        {
            var listaItemA = new List<ItemA>();
            foreach (var item in itemB)
            {
                RetonarPropriedadeCriada(item);
                listaItemA.Add(new ItemA()
                {
                    id = 1,
                    descricao = "Teste"
                });
            }
        }

        private void RetonarPropriedadeCriada(ItemB item)
        { 
            //// Estudar melhor.

            //AssemblyName assemblyName = new AssemblyName("myAssembly");
            //AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            //ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule(assemblyName.Name);
            //TypeBuilder typeBuilder = moduleBuilder.DefineType("Ext_" + item.GetType().Name, TypeAttributes.Public | TypeAttributes.AutoClass | TypeAttributes.AnsiClass | TypeAttributes.BeforeFieldInit, item.GetType());

            ////Property
            //PropertyBuilder propertyBuilder = typeBuilder.DefineProperty(item.PropDesc, PropertyAttributes.None, typeof(decimal),null);

            ////Field
            //FieldBuilder fieldBuilder = typeBuilder.DefineField("_" + item.PropDesc, typeof(decimal), FieldAttributes.Private);

            ////Getter
            //MethodBuilder  

            ////Setter
            //MethodBuilder
            
        }
    }
}
