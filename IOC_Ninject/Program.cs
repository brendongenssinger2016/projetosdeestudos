using System;
using System.Security.Permissions;
using InfraEstrutura.App;
using Ninject;
using Ninject.Modules;

namespace IOC_Ninject
{
    public class Program
    {
        static void Main(string[] args)
        {
            var kernel = new StandardKernel();
            /**
             * Posso da o nome Ex :
             * kernel.Bind<ICreditCard>().To<Visa>().Named("Visa");
             */
            kernel.Bind<ICreditCard>().To<Visa>(); 
            var shopper = kernel.Get<Shopper>();
            shopper.Charge();
            Console.WriteLine(shopper.ChargesForCurrentCartd);
            /**
             * 
             * 
             */

            var shopper2 = kernel.Get<Shopper>();
            shopper2.Charge();
            Console.WriteLine(shopper2.ChargesForCurrentCartd);

            /**
             * Outra forma
             */
             var kernel2 = new StandardKernel();
            kernel2.Bind<ICreditCard>().ToMethod(context => new MasterCard());
            var shopper3 = kernel2.Get<Shopper>();
            shopper3.Charge();
            Console.WriteLine(shopper3.ChargesForCurrentCartd);

            Console.Read();
        }
    }

    public class MyModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ICreditCard>().To<Visa>();
        }
    }
}