﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace UsingFieldsWithReflection
{
    class Program
    {
        static void Main(string[] args)
        {
            var exe = new ExecutarTeste();
            exe.Exe();
        }
    }

    public class ExecutarTeste
    {
        public void Exe()
        {
            var typeOfTheClass = typeof(TesteTwo);
            var testarObject = new TesteTwo();
            foreach (_PropertyInfo p in typeOfTheClass.GetProperties()) // Faço a lista de propriedades da classe TesteTwo.
            {
                //Aqui listo o nome da propriedade e o tipo dela.
                Console.WriteLine($"DadoDaPropriedade : Nome Da Propriedade => {p.Name}/ Dados Dentro da Propriedade => {p.GetValue(testarObject,null)}/ Tipo de propriedade => {p.PropertyType.Name}");
                var list = p.GetCustomAttributes(false);//Crio a lista dos atributos da classe Teste two.
                foreach (var attribute in list)
                {
                    var a = (TesteOne)attribute; // Converto eles pra pegar o nome do field.
                    Console.WriteLine($"Nome do Attributo : {a.NomePropriedade}");
                }
            }
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class TesteOne : Attribute
    {        
        public string NomePropriedade { get; set; }
        private string _teste;
        public void Remover() { }
    }
    
    
    public class TesteTwo
    {
        public TesteTwo()
        {
            Nome = "Brendon****";
        }
        [TesteOne(NomePropriedade = "Nome1")]
        public string Nome { get; set; }
    }
}
