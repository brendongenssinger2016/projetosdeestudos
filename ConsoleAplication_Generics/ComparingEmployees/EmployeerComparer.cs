﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleAplication_Generics.Model;
namespace ConsoleAplication_Generics.ComparingEmployees
{
    public class EmployeerComparer : IEqualityComparer<Employee>, 
                                     IComparer<Employee>
    {
        public int Compare(Employee x, Employee y)
        {
            return String.Compare(x.Name, y.Name);
        }

        public bool Equals(Employee x, Employee y)
        {
            return String.Equals(x.Name, y.Name);
        }

        public int GetHashCode(Employee obj)
        {
            return obj.Name.GetHashCode();
        }
    }

    public class EmployeerUsingCollection
    {
        public static void UsingHashSet()
        {
            
            var departments = new SortedDictionary<string, HashSet<Employee>>();
            departments.Add("Sales", new HashSet<Employee>(new EmployeerComparer()));
            departments["Sales"].Add(new Employee() { Name = "Brendon" });
            departments["Sales"].Add(new Employee() { Name = "Junior" });
            departments["Sales"].Add(new Employee() { Name = "Rafael" });
            departments["Sales"].Add(new Employee() { Name = "Dani" });
            departments["Sales"].Add(new Employee() { Name = "Dani" });
            departments["Sales"].Add(new Employee() { Name = "Dani" });

            departments.Add("Engineering", new HashSet<Employee>(new EmployeerComparer()));
            departments["Engineering"].Add(new Employee() { Name = "Pedro" });
            departments["Engineering"].Add(new Employee() { Name = "Carlos" });
            departments["Engineering"].Add(new Employee() { Name = "joao" });
            departments["Engineering"].Add(new Employee() { Name = "joao" });

            foreach (var item in departments)
            {
                
                Console.WriteLine(item.Key);
                foreach (var funcionariosDepartaments in item.Value)
                {
                    Console.WriteLine("\t" + funcionariosDepartaments.Name);
                    
                }
            }

        }

        public static void UsingSortedSet()
        {
            var departments = new SortedDictionary<string, SortedSet<Employee>>();
            departments.Add("Sales", new SortedSet<Employee>(new EmployeerComparer()));
            departments["Sales"].Add(new Employee() { Name = "Brendon" });
            departments["Sales"].Add(new Employee() { Name = "Junior" });
            departments["Sales"].Add(new Employee() { Name = "Rafael" });
            departments["Sales"].Add(new Employee() { Name = "Dani" });
            departments["Sales"].Add(new Employee() { Name = "Dani" });
            departments["Sales"].Add(new Employee() { Name = "Dani" });

            departments.Add("Engineering", new SortedSet<Employee>(new EmployeerComparer()));
            departments["Engineering"].Add(new Employee() { Name = "Pedro" });
            departments["Engineering"].Add(new Employee() { Name = "Carlos" });
            departments["Engineering"].Add(new Employee() { Name = "joao" });
            departments["Engineering"].Add(new Employee() { Name = "joao" });

            foreach (var item in departments)
            {

                Console.WriteLine(item.Key);
                foreach (var funcionariosDepartaments in item.Value)
                {
                    Console.WriteLine("\t" + funcionariosDepartaments.Name);

                }
            }
        }
    }
}
