﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleAplication_Generics.Model;
namespace ConsoleAplication_Generics.ComparingEmployees
{
    public class EmployeesComparerBestCode : IEqualityComparer<Employee>, IComparer<Employee>
    {
        public int Compare(Employee x, Employee y)
        {
            return String.Compare(x.Name, y.Name);
        }

        public bool Equals(Employee x, Employee y)
        {
            return String.Equals(x.Name, y.Name);
        }

        public int GetHashCode(Employee obj)
        {
            return obj.Name.GetHashCode();
        }
    }

    public class DepartamentCollection:SortedDictionary<string,SortedSet<Employee>>
    {
        public DepartamentCollection Add(string departamentName,Employee employee)
        {
            if(!ContainsKey(departamentName))
            {
                Add(departamentName, new SortedSet<Employee>(new EmployeerComparer()));
            }
            this[departamentName].Add(employee);
            return this;
        }
    }

    public class EmployeesUsingCollectionBestCode
    {
        public static void Ini()
        {
            var departaments = new DepartamentCollection();

            departaments.Add("Sales", new Employee() { Name = "Brendon" })
                .Add("Sales", new Employee() { Name = "Dani" })
                .Add("Engineering", new Employee() { Name = "Paulo" });           

            foreach (var item in departaments)
            {
                Console.WriteLine(item.Key);
                foreach (var itemTwo in item.Value)
                {
                    Console.WriteLine("\t\t" + itemTwo.Name);
                }
            }
        }
    }
}
