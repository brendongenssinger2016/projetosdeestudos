﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleAplication_Generics.ClassesGenericas;
using ConsoleAplication_Generics.Model;

namespace ConsoleAplication_Generics.ClassesGenericas
{
    public static class _Stack
    {
        #region Stack
        public static void Executar()
        {
            Console.WriteLine(".......");
            /*
             * Stack, é pilha e posso pegar algo e depois faço alguma e depois em seguida
             * faço outra coisa.
             */
            Stack<Employee> stack = new Stack<Employee>();
            stack.Push(new Employee { Name = "Jonas" });
            stack.Push(new Employee { Name = "Dani" });
            stack.Push(new Employee { Name = "Chris" });
            while (stack.Count > 0)
            {
                var employe = stack.Pop();
                Console.WriteLine(employe.Name);
            }
        }
        #endregion
    }
}
