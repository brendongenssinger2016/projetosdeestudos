﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAplication_Generics.ClassesGenericas
{
    public static class _LinkedList
    {
        #region LinkedList
        /*
         * Tenho uma lista o AddFirst é adicionar o primeiro da lista, caso contem algum algo adiiconado antes 
         * ele passará a ser o segundo.
         * Caso eu adiciono usando o metodo before e passo o paramentro do primeiro da lista, o númeor que estiver 
         * adicionando passará a ser o primeiro, caso seja com o metodo afther ele será o segundo.s         * 
         */

        public static void Executar()
    {
        LinkedList<int> list = new LinkedList<int>();
        list.AddFirst(2);
        list.AddFirst(3);
        list.AddBefore(list.First, 5);

        var first = list.First;
        list.AddAfter(first, 6);

        foreach (var item in list)
        {
            Console.WriteLine(item);
        }

    }
    #endregion
}
}
