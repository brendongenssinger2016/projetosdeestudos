﻿using ConsoleAplication_Generics.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAplication_Generics.ClassesGenericas
{
    public class _Dicionary
    {
        /* O dicionário serve pra você buscar algum valor pela chave, pode ser qualquer objeto.*/
        /* Temos o SortedDictionary que ordena por ordem crescente e alfabetica. */

        Dictionary<string,string> textoStrings = new Dictionary<string, string>();
        Dictionary<char,char> letrasDictionary = new Dictionary<char, char>();
        
        public static void Executar()
        {
            var employByName = new Dictionary<string, Employee>();
            employByName.Add("Brendon", new Employee { Name = "Brendon" });
            employByName.Add("Joao", new Employee { Name = "Joao" });

            foreach (var item in employByName)
            {
                Console.WriteLine(string.Format("{0}{1}", item.Key, item.Value.Name));
            }
        }

        public static void ExecutarLista()
        {
            var employByDepartament = new Dictionary<string, List<Employee>>();
            employByDepartament.Add("Suporte", new List<Employee>() { new Employee { Name = "Brendon" } });
            employByDepartament["Suporte"].Add(new Employee { Name = "Joao" });
            employByDepartament.Add("Financeiro", new List<Employee>() { new Employee { Name = "Slack" } });

            foreach (var item in employByDepartament)
            {
                Console.WriteLine(item.Value + item.Key);
                foreach (var employee in item.Value)
                {
                    if (item.Key.Equals("Suporte"))
                        Console.WriteLine(employee.Name);
                    else if (item.Key.Equals("Financeiro"))
                        Console.WriteLine("Financeiro" + employee.Name);
                }
            }
        }
        public _Dicionary()
        {
            if(textoStrings == null)
            { 
                textoStrings.Add("abcdefghijklmnopqrstuvxzçá", "zxvutsrqponmlkjihgfedcbaçá");
                textoStrings.Add("0123456789", "9876543210");
            }
        }
        //public static void ExectuarTesteProva(string textoInformado)
        //{
            
        //    var textA = new List<char>();
        //    var textB = new List<char>();         
        //    string traducao = "";

        //    foreach (var chaves in textoStrings.Keys) // Chave inteira
        //    {
        //        foreach (var porCadaChave in chaves.ToUpper()) // por cada chave
        //        {
        //            textA.Add(porCadaChave);
        //        }
        //    }
        //    foreach (var text in textoStrings.Values) // letras inteira
        //    {
        //        foreach (var letrasDeString in text.ToUpper())//por cada letra
        //        {
        //            textB.Add(letrasDeString);
        //        }                
        //    }
        //    for (int i = 0; i < textA.Count; i++)
        //    {
        //        letrasDictionary.Add(textA[i], textB[i]);
        //    }
        //    foreach (var percorrendoCadaLetra in textoInformado.ToUpper())
        //    {
        //        if (char.IsWhiteSpace(percorrendoCadaLetra))
        //        {
        //            traducao = traducao + percorrendoCadaLetra;
        //        }
        //        else
        //        {
        //            var valorDaLetra = letrasDictionary[percorrendoCadaLetra];
        //            traducao = traducao + valorDaLetra;
        //        }
        //    }
        //    Console.WriteLine(traducao.ToLower());
        //}

        public static void traducaoPortuguesParaIngles()
        {   
        }
    }
}
