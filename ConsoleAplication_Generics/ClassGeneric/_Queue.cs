﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleAplication_Generics.Model;
namespace ConsoleAplication_Generics.ClassesGenericas
{
    public static class _Queue
    {
        #region Queue
        public static void Executar()
        {
            Queue<Employee> line = new Queue<Employee>(); //Fila de Pessoas, o primeiro, segundo terceiro.
            line.Enqueue(new Employee { Name = "Brendon" });
            line.Enqueue(new Employee { Name = "Joao" });
            line.Enqueue(new Employee { Name = "Sara" });
            Console.WriteLine("Class Generic QUEUE");
            while (line.Count > 0)
            {
                var employes = line.Dequeue();
                Console.WriteLine(employes.Name);
            }
        }
        #endregion
    }
}
