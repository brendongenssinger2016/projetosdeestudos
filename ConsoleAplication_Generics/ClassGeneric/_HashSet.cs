﻿using ConsoleAplication_Generics.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAplication_Generics.ClassesGenericas
{
    public static class _HashSet
    {
        #region HashSet
        public static void Executar()
        {
            HashSet<Employee> set = new HashSet<Employee>();
            set.Add(new Employee { Name = "Joao" });
            set.Add(new Employee { Name = "Joao" });
            foreach (var item in set)
            {
                Console.WriteLine(item.Name);
            }
            Console.Read();
        }
        #endregion
    }
}
