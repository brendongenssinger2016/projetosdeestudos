﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Caliburn.Micro;
namespace DesenvolvendoWpfAvançado.Auxiliares
{
    public static class XThread
    {
        [STAThread]
        public static void ExecutarThread(System.Action pAction, Action<Exception> ex)
        {            
            new Thread(new ThreadStart(() => 
            {
                try
                {
                    pAction();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                
            })).Start();
        }
    }
}
