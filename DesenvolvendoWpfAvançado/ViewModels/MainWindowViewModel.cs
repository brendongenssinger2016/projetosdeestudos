﻿using Caliburn.Micro;
using DesenvolvendoWpfAvançado.Auxiliares;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DesenvolvendoWpfAvançado.ViewModels
{
    
    public class MainWindowViewModel : Conductor<IScreen>.Collection.OneActive, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public bool _isEnable;
        private double _valorOpacity;

        [STAThread]
        public void BtnTeste()
        {
            ActivateItem(new LoadOneViewModel());
            var mainForm = new LoadOneViewModel();
            ActivateItem(mainForm);
            //ValorOpacity = 1;
            //isEnableBorder = false;            
            //NotifyOfPropertyChange(() => isEnableBorder);
            //Execute.OnUIThread(() =>            
            //{
            //    Mouse.OverrideCursor = Cursors.Wait;
            //    Thread.Sleep(10000);
            //    BackgroundWorker bgWork = new BackgroundWorker()
            //    {
            //        WorkerReportsProgress = true
            //    };
            //    bgWork.DoWork += (s, e) =>
            //     {
            //         bgWork.ReportProgress(10);

            //     };
            //    //bgWork.ProgressChanged += (s, e) =>
            //    //{
            //    //    Mouse.OverrideCursor = null;
            //    //};
            //    bgWork.RunWorkerAsync();
            //    Mouse.OverrideCursor = null;
            //});
            //isEnableBorder = true;
            //NotifyOfPropertyChange(() => isEnableBorder);
        }        

        public MainWindowViewModel()
        {
            TypeCursor = CursorType.None;
            isEnableBorder = true;
        }
        public CursorType TypeCursor { get; set; }

        public bool isEnableBorder
        {
            get
            {
                return _isEnable;
            }
            set
            {
                if (_isEnable == value) return;
                _isEnable = value;
                NotifyOfPropertyChange(() => isEnableBorder);

            }
        }

        public void OnPropertyChanged(object sender, EventArgs e)
        {

        }
        public double ValorOpacity
        {
            get { return _valorOpacity; }
            set
            {
                _valorOpacity = value;
            }
        }

    }
}
