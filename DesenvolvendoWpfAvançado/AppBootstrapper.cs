﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using DesenvolvendoWpfAvançado.ViewModels;
namespace DesenvolvendoWpfAvançado
{
    public class AppBootstrapper : BootstrapperBase
    {
        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainWindowViewModel>();
        }
        public AppBootstrapper()
        {
            Initialize();
        }
    }
}
