﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace UsingMongoDB
{
    class Program
    {
        static void Main(string[] args)
        {
            var cliente = new MongoClient("mongodb://localhost:27017/");
            var dataBase = cliente.GetDatabase("TestePrimeiraDataBase");
            var collection = dataBase.GetCollection<BsonDocument>("teste");
            var document = new BsonDocument
            {
                { "name","MongDB"},
                {"type","Database" },
                {"count",1 },
                {"info",new BsonDocument
                {
                    {"x",203 },
                    {"y",102 }
                } }
            };
            collection.InsertOne(document);
            var listDeCollection = collection.Find(new BsonDocument()).ToList();
            foreach (var text in listDeCollection)
            {
                Console.WriteLine(text.Names);
            }
            Console.Read();
        }
    }
}
