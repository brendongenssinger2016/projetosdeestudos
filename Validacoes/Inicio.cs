﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validacoes
{
    public static class Inicio
    {
        private static Pessoas pessoas;
        public static void Main(string[] args)
        {
            pessoas = new Pessoas();
            
            var listTabs = new Dictionary<Action,Action>();

            listTabs.Add(Tab1,ValidaTab1);
            listTabs.Add(Tab2, ValidaTab2);

            foreach (var item in listTabs)
            {
                var result = string.Empty;
                bool prox=false;

                while(string.IsNullOrEmpty(result) || result.ToUpper().Equals("N") || !prox)
                {
                    item.Key();
                    Console.WriteLine("Executar proximo tab");
                    result = Console.ReadLine();
                    if (result.ToUpper().Equals("S"))
                    {
                        prox =  ValidaCampos(item.Value);
                    }                                    
                }

                if (prox) continue;
            }

            Console.ReadLine();
        }

        private static void ValidaTab2()
        {
            if (pessoas.DataNascimento == null)
                throw new Exception("Data de nascimento não informada");
        }

        private static void Tab2()
        {
            Console.WriteLine("*** Tab 2 ***");

            Console.WriteLine("Informe Data de Nascimento");
            var date = Console.ReadLine();
            if(date != null && !string.IsNullOrEmpty(date))
                pessoas.DataNascimento =Convert.ToDateTime(date);
        }

        private static void ValidaTab1()
        {
            if (string.IsNullOrEmpty(pessoas.Nome))
                throw new Exception("Nome não informado.");

            if (string.IsNullOrEmpty(pessoas.SobreNome))
                throw new Exception("Sobrenome não informado.");
        }

        public static bool ValidaCampos(Action pValida)
        {
            try
            {
                pValida();
                return true;
            }
            catch (Exception eX)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Alguns campos não foram validados.\n"+eX.Message);
                Console.ForegroundColor = ConsoleColor.White;
                return false;
            }
        }


        public static void Tab1()
        {
            Console.WriteLine("*** Tab 1 ***");

            Console.WriteLine("Informe nome");
            pessoas.Nome = Console.ReadLine();
            
            Console.WriteLine("Informe sobrenome");
            pessoas.SobreNome = Console.ReadLine();
        }
    }

    public class Pessoas
    {
        public string Nome { get; set; }
        public string SobreNome { get; set; }
        public DateTime DataNascimento { get; set; }
        public string email { get; set; }
    }
}
