﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Windsor;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using InfraEstrutura.App;
namespace CastleWindsor
{
    class Program
    {
        static void Main(string[] args)
        {
            Aula3Modulo6();
        }

        public static void Aula3Modulo6()
        {
            var container = new WindsorContainer();
            container.Install(new ShoppingInstaller());
            var shopper = container.Resolve<OverrideShopper>();
            var shopper2 = container.Resolve<OverrideShopper>();
            shopper.Charge();
            shopper2.Charge();
            
            Console.WriteLine(shopper.ChargesForCurrentCartd);
            Console.WriteLine(shopper2.ChargesForCurrentCartd);
            Console.ReadKey();

        }

    }

    internal class ShoppingInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<OverrideShopper>());
            container.Register(Component.For<ICreditCard>().ImplementedBy<MasterCard>());
        }
    }
}
