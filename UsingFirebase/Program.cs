﻿using FireSharp;
using FireSharp.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsingFirebase
{
    class Program
    {
        static void Main(string[] args)
        {
            var firebase = new FirebaseClient(new FirebaseConfig()
            {
                Host = @"https://estudosfirebase-7d19b.firebaseio.com/",
                RequestTimeout = new TimeSpan(0, 10, 0)                

            });

            try
            {
                var teste = new Teste() 
                {
                    Key = 1
                };

                var dinos = firebase.Push<Teste>("estudosfirebase-7d19b", teste).Body;

            }
            catch (Exception eX)
            {

                throw;
            }


            Console.ReadKey();
        }
    }

    public class Teste
    {
        public int Key { get; set; }
    }
}
