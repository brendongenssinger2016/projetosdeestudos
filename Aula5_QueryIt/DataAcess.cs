﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;

namespace Aula5_QueryIt
{
    public class EmployeeDB : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
    }

    public interface IReadyOnllyRepository<out T> : IDisposable
    {
        T FindById(int id);
        IQueryable<T> FindAll();
    }

    public interface IWriteOnllyRepository<in T>: IDisposable
    {
        void Add(T newEntity);
        void Delete(T newEntity);
        int Commit();
    }

    
    public interface IRepository<T> : IDisposable , IReadyOnllyRepository<T>, IWriteOnllyRepository<T>
    {
        
    }

    // Clausula Where especifica que é apenas tipo de referencia.
    public class SqlRepository<T> : IRepository<T> where T: class , IEntity
    {
        private DbContext _context;
        private DbSet<T> _set;
        public SqlRepository(DbContext ctx)
        {
            _context = ctx;
            _set = _context.Set<T>();
        }

        public void Add(T newEntity)
        {
            _set.Add(newEntity);
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        public void Delete(T newEntity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> FindAll()
        {
            return _set;
        }

        public T FindById(int id)
        {
            return _set.Find(id);
        }       
        public void Dispose()
        {
            _context.Dispose(); 
        }
        

    }

}