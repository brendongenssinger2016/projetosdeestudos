﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aula5_QueryIt
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<EmployeeDB>());            

            using (IRepository<Employee> employeeRepository = 
                new SqlRepository<Employee>(new EmployeeDB()))
            {
                AddEmployees(employeeRepository);
                AddManager(employeeRepository);
                CountEmployees(employeeRepository);
                QueryEmployees(employeeRepository);
                DumpPeople(employeeRepository);
            }

            Console.Read();
        }

        private static void AddManager(IWriteOnllyRepository<Manager> employeeRepository)
        {
            employeeRepository.Add(new Manager { Names = "Alex" });
            employeeRepository.Commit();
        }

        private static void QueryEmployees(IRepository<Employee> employeeRepository)
        {
            var employe = employeeRepository.FindById(1);
            Console.WriteLine(employe.Names);
        }

        private static void DumpPeople(IReadyOnllyRepository<Employee> employeeRepository)
        {
            var employess = employeeRepository.FindAll();
            foreach (var employe in employess)
            {
                 Console.WriteLine(employe.Names);
            }
        }

        private static void CountEmployees(IRepository<Employee> employeeRepository)
        {
            Console.WriteLine(employeeRepository.FindAll().Count());
        }

        private static void AddEmployees(IRepository<Employee> employeeRepository)
        {
            employeeRepository.Add(new Employee { Names = "Brendon" });
             employeeRepository.Add(new Employee { Names = "Sara" });
            employeeRepository.Commit();
        }
    }
}
