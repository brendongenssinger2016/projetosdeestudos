﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataGridViewWPF
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var row = new RowDefinition();
            row.DataContext = "Teste1";
            for (int i = 0; i < 2000; i++)
            {
                _DataGrid.Items.Add(new MyData
                {
                    title1 = $"Teste Linha {i}",
                    title2 = $"Teste Linha {i}",
                    title3 = $"Teste Linha {i}",
                    title4 = $"Teste Linha {i}",
                    title5 = $"Teste Linha {i}",
                    title6 = $"Teste Linha {i}"
                });
            }
        }

    }

    public class MyData
    {
        public string title1 { set; get; }
        public string title2 { set; get; }
        public string title3 { set; get; }
        public string title4 { set; get; }
        public string title5 { set; get; }
        public string title6 { set; get; }

    }
}