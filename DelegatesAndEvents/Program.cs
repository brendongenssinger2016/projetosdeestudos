﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAndEvents
{
    public class Program
    {
        public delegate void WorkPerformedHandler(int hours, WorkType workType);
        public static void Main(string[] args)
        {
            WorkPerformedHandler del1 = new WorkPerformedHandler(WorkPerformed1);
            WorkPerformedHandler del2 = new WorkPerformedHandler(WorkPerformed2);
            Console.CursorVisible = false;
            DoWork(del2);
            del1 += del2;
            del2 += del1;
            del1(5, workType: WorkType.Golf);
            Console.Read();
        }

        public static void DoWork(WorkPerformedHandler del)
        {
            del(5, WorkType.Golf);
        }

        static void WorkPerformed1(int hours, WorkType workType)
        {
            Console.WriteLine("WorkPerformed1 called" + hours.ToString());
        }

        static void WorkPerformed2(int hours, WorkType workType)
        {
            Console.WriteLine("WorkPerformed2 called" + hours.ToString());
        }
    }

    internal enum WorkType
    {
        GotoMeetings,
        Golf
    }

    public class Work
    {
        public event WorkPerformedHandler WorkPerformed;
        public event EventHandler WorkCompleted;
    }
}
