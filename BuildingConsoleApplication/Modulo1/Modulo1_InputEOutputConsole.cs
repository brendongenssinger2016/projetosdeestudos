﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildingConsoleApplication.Modulo1
{
    public class Modulo1_InputEOutputConsole
    {
        public void Executar()
        {
            var inputFileName = Path.Combine(Environment.CurrentDirectory, "Modulo1/Names.txt");
            var inputNames = new StreamReader(inputFileName);

            while (true)
            {
                var text = inputNames.ReadLine();
                    if(text != null)
                        Console.WriteLine(text);
                    else
                        break;
            }
            

        }
    }
  
}
