﻿using System;

namespace BuildingConsoleApplication.Modulo1
{
    class Modulo1CheackingNumLockOpen
    {
        public void ExecutarTeste()
        {
            var k = Console.ReadKey(true);
            if (Console.CapsLock && Console.NumberLock)
            {
                Console.WriteLine(k.KeyChar);
            }
        }
	}
    }

