﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delegates.UsingFunc;
namespace Delegates.UsingFunc
{
    public class StaticFunc
    {
        public static void UsingFunc()
        {
            Func<int, int, int> funcAddDel = (x, y) => x + y;
            Func<int, int, int> funcMultiPly = (x ,y) => x * y;
            var data = new ProcessData();
            Func<int,int,int> funcAdd = (x,y)=> x + y;
            data.ProcessFunc(2, 3, (x, y) => x + y);
        }
    }
}
