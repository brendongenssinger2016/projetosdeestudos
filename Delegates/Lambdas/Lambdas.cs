﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delegates.Model;

namespace Delegates.Lambdas
{
    public class Lambdas
    {
        public static void ExecutarLambda()
        {
            var custs = new List<Customer>
            {
                new Customer{City = "Phoenix",FirstName = "John",LastName = "Ultimo Nome",ID = 1},
                new Customer{City = "Goiania",FirstName = "Brendon",LastName = "Ultimo Nome",ID = 2},
                new Customer{City = "Aparecida de Goiania",FirstName = "Brendon",LastName = "Ultimo Nome",ID = 3}
            };

            var phxCusts = custs
                .Where(c => c.City == "Phoenix" && c.ID==1)
                .OrderBy(c=> c.City);
            foreach (var cust in phxCusts)
            {
                Console.WriteLine(cust.FirstName);
            }

        }
    }
}
