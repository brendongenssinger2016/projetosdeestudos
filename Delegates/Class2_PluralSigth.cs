﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    public class Class2_PluralSigth
    {
        public delegate void WorkPerformedHandler(int hours, WorkType workType);
        public static void Rodar()
        {
            WorkPerformedHandler work = new WorkPerformedHandler(WorkPerformed1);
            WorkPerformedHandler work2 = new WorkPerformedHandler(WorkPerformed2);
            WorkPerformedHandler work3 = new WorkPerformedHandler(WorkPerformed3);

            work += work2;
            work += work3;
            work(3, WorkType.Golf);
            //work(3, WorkType.GoToMeeTings);
            //work2(4, WorkType.Golf);
            //dowork(work);
        }

        static void Dowork(WorkPerformedHandler del)
        {
            del(3, WorkType.Golf);
        }

        static void WorkPerformed1(int hours, WorkType workType)
        {
            Console.WriteLine("WorkPerforme1 Called {0}", hours.ToString());
        }

        static void WorkPerformed2(int hours, WorkType workType)
        {
            Console.WriteLine("WorkPerforme2 Called {0}", hours.ToString());
        }
        static void WorkPerformed3(int hours, WorkType workType)
        {
            Console.WriteLine("WorkPerforme3 Called {0}", hours.ToString());
        }
    }
}
