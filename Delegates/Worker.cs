﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    //public delegate void WorkPerformedHandler(Object sender, WorkPerformedEventArgs e);

    public class Worker
    {
        public event EventHandler<WorkPerformedEventArgs> WorkPerformed;
        public event EventHandler WorkCompleted;
        public void OnWorkPerformed(int hours, WorkType workType)
        {
            var del = WorkPerformed as EventHandler<WorkPerformedEventArgs>;
            if (del != null)
            {
                del(this,new WorkPerformedEventArgs(hours,workType));
            }
            //OnWorkCompleted();
        }

        public void DoWork(int hours, WorkType worktype)
        {
            for (int i = 0; i < hours; i++)
            {
                System.Threading.Thread.Sleep(1000);
                OnWorkPerformed(i+1, worktype);
            }
            OnWorkCompleted();
        }

        private void OnWorkCompleted()
        {
            var del = WorkCompleted as EventHandler;
            if(del != null)
                del(this,EventArgs.Empty);
        }
    }
}
