﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Delegates.UsingFunc;
namespace Delegates
{
    public delegate void WorkPerformedHandler(int hours, WorkType worktyep);
    public delegate int BizRulesDelegate(int x, int y);
    class Program
    {
        
        public static void Main(string[] args)
        {
            //WorkPerformedHandler work_ = new WorkPerformedHandler(teste);
            //work_(6, WorkType.GoToMeeTings);
            //Console.Read();
            Lambdas.Lambdas.ExecutarLambda();

            int AddDel(int x, int y) => x + y;

            int MultiplyDel(int x, int y) => x * y;

            var data = new ProcessData();
            data.Process(2,3,AddDel);
            data.Process(2, 3, MultiplyDel);

            /*Using Action and Action<T,T>*/
            void Myaction(int x, int y) => Console.WriteLine("Action myAction : " + x + y);
            Action<int, int> myMultiplyAction = (x, y) => Console.WriteLine(x*y);
            data.ProcessAction(2, 3, Myaction);

            /*Using FUNC<T,T>*/
            StaticFunc.UsingFunc();

            var worker = new Worker();
            worker.WorkPerformed += (s, e) => { Console.WriteLine($"Hours worked: {e.Hours} {e.WorkType}"); };
            worker.WorkCompleted += Worker_WorkCompleted;
            worker.WorkCompleted -= Worker_WorkCompleted;
            worker.DoWork(8, WorkType.GenerateReports);
            Console.Read();
        }

        //static void worker_WorkPerformed(object sender, WorkPerformedEventArgs e)
        //{
        //    //throw new NotImplementedException();

        //    Console.WriteLine($"Hours worked: {e.Hours} {e.WorkType}");
        //}

        public static void Worker_WorkCompleted(object sender, EventArgs e)
        {
            Console.WriteLine($"Worker is done");
        }
        public static void Teste(int hours, WorkType worktype)
        {
            Console.WriteLine("Metodo Iniciado..");
            Console.Read();
        }
    }

    public enum WorkType
    {
        GoToMeeTings,
        Golf,
        GenerateReports
    }
}
