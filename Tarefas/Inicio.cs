﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tarefas.Negocios;
namespace Tarefas
{
    public static class Inicio
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Criar (Crud) de Pessoas");
            NegociosBLL.Instancia.Cadastrar(new Model.Pessoa()
            {
                Nome = "Brendon",
                SobreNome = "Mascarenhas",
                Cidades = Enumerados.eCidades.Goias,
                DataNascimento = DateTime.Now
            });

            NegociosBLL.Instancia.Cadastrar(new Model.Pessoa()
            {
                Nome = "Alvaro",
                SobreNome = "Cunha",
                Cidades = Enumerados.eCidades.Maranhao,
                DataNascimento = DateTime.Now
            });

        }
    }
}
