﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tarefas.Enumerados;

namespace Tarefas.Model
{
    public class Pessoa
    {
        public string Nome { get; set; }
        public string SobreNome { get; set; }
        public eCidades Cidades { get; set; }
        public DateTime DataNascimento { get; set; }

    }
}
