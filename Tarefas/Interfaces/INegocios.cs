﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tarefas.Model;
namespace Tarefas.Interfaces
{
    public interface INegocios
    {
        void Cadastrar(Pessoa pPessoa);
        void Excluir(Pessoa pPessoa);
        void Alterar(string pNome, Pessoa pPessoa);
        Pessoa[] ListPessoas();

    }
}
