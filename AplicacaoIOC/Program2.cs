﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacaoIOC.Program2
{
    // Aplicação de inversão de controle.
    public interface IService2
    {
        void Server();
    }
    public class Service2 : IService2
    {
        public void Server()
        {
            Console.WriteLine("Service Called");
        }
    }

    public class Cliente2
    {
        private IService2 _service;
        public Cliente2(IService2 service)
        {
            _service = service;
        }
        public void Start()
        {
            Console.WriteLine("Serviço estartado");
            this._service.Server();
        }

    }
}
