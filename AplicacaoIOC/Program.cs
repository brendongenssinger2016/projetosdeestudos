﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AplicacaoIOC.Program2;
namespace AplicacaoIOC
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //Cliente cliente = new Cliente();
            //cliente.Start();

            Cliente2 cliente = new Cliente2(new Service2());
            cliente.Start();
        }
    }
    public interface IService
    {
        void Serve();
    }
    public class Service : IService
    {
        public void Serve()
        {
            Console.WriteLine("Service Called");
        }
    }

    public static class LocateService
    {
        public static IService _Service { get; set; }
        public static IService GetService()
        {
            if (_Service is null)
                _Service = new Service();
            return _Service;
        }
    }
    public class Cliente
    {
        private IService _service;
        public Cliente()
        {
            this._service = LocateService.GetService();
        }
        public void Start()
        {
            Console.WriteLine("Service Startado");
            this._service.Serve();
        }
    }

    

    
}
