﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formats.Developers
{
    public class Program
    {
        static void Main(string[] args)
        {
            var number = 1212;
            var tiposDeFormats = new string[]
            {
                "00000000","000000","00000","0.00","[##-##-##]","[(##)]####","MMMM dd, yyyy","ddd"
            };
            foreach (var item in tiposDeFormats)
            {
                Console.WriteLine($"TypeFormat {item}");
                Console.WriteLine($"Value => {number}");
                Console.WriteLine($"Out Value => {number.ToString(item)}");
                Console.WriteLine("\n");
            }
            Console.ReadKey();
        }
    }
}
