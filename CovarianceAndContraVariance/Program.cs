﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovarianceAndContraVariance
{
    // Covariance with delegate
    public class Program
    {
        public delegate Small CovarDel(Big mc);

        static Big Method1(Big bg)
        {
            Console.WriteLine("Method1");
            return new Big();
        }

        static Small Method2(Big bg)
        {
            Console.WriteLine("Method2");
            return new Small();
        }
        public static void Main(string[] args)
        {
            CovarDel del = Method1;
            del += Method2;
            var sm = del(new Big());
            
            Console.ReadKey();
        }
    }
    //public interface ISmall
    //{ }

   public class Small /*: ISmall*/
    {
        
    }

    public class Big : Small
    {
        
    }

   public class Bigger: Big
    {
        
    }


}
