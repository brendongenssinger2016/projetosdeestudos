﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsingFuncActionAndPredicate
{
    class Program
    {
        static void Main(string[] args)
        {
         

            Console.WriteLine("Digite a operação que deseja utilizar ?\n" +
                              "[1] Uso do Func \n" +
                              "[2] Uso do Action \n" +
                              "[3] Uso do Predicate\n");
            var operacao = Convert.ToInt32(Console.ReadLine());
            switch (operacao)
            {
                case 1:
                    GetValueUsingFunc();
                    break;
                case 2:
                    GetValueUsingAction(string.Empty);
                    break;
                case 3:
                    GetValuesUsingPredicate();
                    break;
                default:
                    Console.WriteLine("Nenhuma opção informada corretamente.");
                    break;
            }
        }

        #region Func
        /*
          * Func :               
             Func é usado para determinar um delegate. 
             Ou seja é para tipar (criar uma assinatura) uma função anônima. 
             Nele é especificado os tipos de diversos parâmetros e o tipo do retorno da função.
             EXAMPLE:
          */

        private static void GetValueUsingFunc()
        {
            Console.WriteLine("Digite a conta.");
            var dadoDigitado = Console.ReadLine().TrimStart(' ');
            var val1 =Convert.ToInt32(char.GetNumericValue(dadoDigitado, 0));
            var val2 = Convert.ToInt32(char.GetNumericValue(dadoDigitado, 2));
            var val3 = dadoDigitado.Replace(val1.ToString(), "").Replace(val2.ToString(), "");
            var operacoes = new Dictionary<string, Func<int, int, int>>
            {
                {"+", (op1, op2) => op1 + op2},
                {"*", (op1, op2) => op1 * op2},
                {"/", (op1, op2) => op1 / op2},
                {"-", (op1, op2) => op1 - op2}
            };
            if(operacoes.ContainsKey(val3))
                Console.WriteLine("Resultado: {0}",operacoes[val3](val1,val2));
            else
                Console.WriteLine("Conta invalida digitada.");
        }
        #endregion

        #region Action
        /*Action
         Action é uma Func que não terá um retorno, ou seja, 
         é função anônima que retorna nada (seria o tipo void). 
         Ela faz uma ação ao invés de dar um resultado, como costuma acontecer com funções. 
         * 
         * 
         */
        private static void GetValueUsingAction(string acao)
        {
            var acoes = new Dictionary<string, Action<int>>
            {
                { "Criar",(paramentro)=>Criar(paramentro)},
                { "Editar",(paramentro)=>Editar(paramentro)},
                { "Apagar",(paramentro)=>Apagar(paramentro)},
                { "Imprimir",(paramentro)=>Imprimir(paramentro)}
            };
            if (acoes.ContainsKey(acao))
                Console.WriteLine("Inicianco a acao {0}", acoes[acao]);
        }

        private static void Imprimir(int paramentro)
        {
            throw new NotImplementedException();
        }

        private static void Apagar(int paramentro)
        {
            throw new NotImplementedException();
        }

        private static void Editar(int paramentro)
        {
            throw new NotImplementedException();
        }

        private static void Criar(int paramentro)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Predicate
        /*Predicate
          Predicate é uma Func que retorna um bool. 
          Hoje ele não é muito necessário. 
          Func resolve bem. 
          Só use se quiser realmente indicar que aquilo não é uma função qualquer, 
          mas sim um predicado (critério para um filtro). Predicate só pode ter um parâmetro. 
          Os dois tipos anteriores permitem até 16 parâmetros já que existem vários tipos com assinaturas diferentes.
         * 
         * 
         * 
         * 
         */
        private static void GetValuesUsingPredicate()
        {
            var compareZero = new Dictionary<string, Predicate<int>>
            {
                {">",(x) => x >0 },
                {"<",(x)=> x < 0 },
                {"==",(x) => x==0 }
            };
        }
        #endregion
        
    }
}
