﻿using EstudosEntityFrameork.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudosEntityFrameork
{
    class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                var context = new NoticiasContext();
                context.Noticias.Add(new Model.Noticias()
                {
                    Id = 1,
                    Noticia = "Not 1"
                });

                context.SaveChanges();
                

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
