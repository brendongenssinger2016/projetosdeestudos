﻿using EstudosEntityFrameork.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudosEntityFrameork.Context
{
    public class NoticiasContext : DbContext
    {
        public NoticiasContext() : base("Server=ENGE056;DATABASE=Noticias;User Id = Engegraph;Password=DevEngegraph")
        {
        }


        public DbSet<Noticias> Noticias { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {            
            base.OnModelCreating(modelBuilder);            
        }
    }
}
 