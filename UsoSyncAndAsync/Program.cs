﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UsoSyncAndAsync
{
    class Program
    {
        static void Main(string[] args)
        {
            //ExecutarProgram();

            // Quebrar a senha.

            var tempoQueComecou = Stopwatch.StartNew();
            var dict = new Dictionary<int,int>();
            int rando = 0;
            var _thread = new Thread(() => { rando = UsarThread(dict); });
            var _thread2 = new Thread(() => { rando = UsarThread(dict); });
            var _thread3 = new Thread(() => { rando = UsarThread(dict); });
            var _thread4 = new Thread(() => { rando = UsarThread(dict); });
            _thread.Priority = ThreadPriority.Normal;
            _thread2.Priority = ThreadPriority.Normal;
            _thread3.Priority = ThreadPriority.Normal;
            _thread4.Priority = ThreadPriority.Highest;
            _thread.Start();
            _thread2.Start();
            _thread3.Start();
            _thread4.Start();

            tempoQueComecou.Stop();

            Console.WriteLine(tempoQueComecou.Elapsed.Hours + "," + tempoQueComecou.Elapsed.Minutes + "," +
                              tempoQueComecou.Elapsed.Seconds);
            Console.WriteLine(rando);

            Console.ReadLine();
        }

        private static int UsarThread(Dictionary<int, int> dict)
        {
            int rando = 0;
            do
            {
                var _rando = new Random();
                if (dict.ContainsKey(rando))
                    rando = _rando.Next(1, 9999);
                else
                    dict.Add(rando, rando);
                if (rando == 1934)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(rando);
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(rando);
                }
            } while (rando != 1934);
            return rando;
        }
        private static void ExecutarProgram()
        {
            int _valor = 0;
            var rand = new Random();

            do
            {
                _valor = rand.Next(1, 30);
                Console.WriteLine(_valor);
                TesteAsync(_valor);
            } while (_valor != 1000);

            Console.Read();
        }

        static async void TesteAsync(int i)
        {
            Console.WriteLine("Aguarde...");
            var result = await retornarTrueORFalse(i);
            Console.WriteLine("Resultado" + result);
        }

        static Task<bool> retornarTrueORFalse(int i)
        {
            if (i == 10)
                return Task.FromResult(true);
            else
            {
                return Task.FromResult(false);
            }
        }
    }
}
