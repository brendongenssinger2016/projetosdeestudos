﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace StudyMongoDB
{
    class Program
    {
        static void Main(string[] args)
        {
            CallMain(args).Wait();
            Console.ReadLine();

        }

        static async Task CallMain(string[] args)
        {
            try
            {
                var settings = new MongoClientSettings
                {
                    ServerSelectionTimeout = new TimeSpan(0, 0, 5),
                    Server = new MongoServerAddress("localhost", 27017)
                };

                var client = new MongoClient(settings);
                var database = client.GetDatabase("startup_log");
                var colecao = database.GetCollection<BsonDocument>("startup_log");
                var _list = await colecao.Find(new BsonDocument()).ToListAsync();
                foreach (var l in _list)
                {
                    Console.WriteLine(l);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }

    
    public class StartupLog
    {
        [BsonElement("_id")]
        [BsonRequired()]
        public string _id { get; set; }
        [BsonElement("hostname")]
        [BsonRequired()]
        public string hostname { get; set; }
        [BsonElement("startTime")]
        [BsonRequired()]
        public DateTime startTime { get; set; }
        [BsonElement("startTimeLocal")]
        [BsonRequired()]
        public string startTimeLocal { get; set; }
    }

    public class ListDoBanco
    {
        private List<StartupLog> GetDados;

        public void AddList(StartupLog pStartupLog)
        {
            GetDados.Add(pStartupLog);
        }
    }
}
