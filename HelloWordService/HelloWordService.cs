﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace HelloWordService
{
    [DataContract]
    public class Name
    {
        [DataMember]
        public string First { get; set; }
        [DataMember]
        public string Last { get; set; }

    }
    [ServiceContract]
    public interface IHelloWord
    {
        [OperationContract]
        string SayHello(Name name);
    }

    public class HelloWordService : IHelloWord
    {
        public string SayHello(Name name)
        {
            return string.Format("First Name: {0} Last Name:{1}",name.First, name.Last);
        }
    }
}
