﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsingUnityIOC
{
    public class Visa: ICreditCard
    {
        public string Charge()
        {
            return "Visa... Visa";
        }

        public int ChargeCount
        {
            get { return 0; }
        }

        public string creditcard()
        {
            return "Visa... Visa";
        }
    }
}
