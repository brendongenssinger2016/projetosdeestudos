﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsingUnityIOC
{
    public interface ICreditCard
    {
        string Charge();
        int ChargeCount { get; }
    }
}
