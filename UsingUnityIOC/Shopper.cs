﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsingUnityIOC
{
   public class Shopper
   {
       private readonly ICreditCard creditCard;

       public Shopper(ICreditCard creditCard)
       {
           this.creditCard = creditCard;
       }

       public int ChargesForCurrentCartd
       {
            get { return creditCard.ChargeCount; }
       }
       public void Charge()
       {
           Console.WriteLine(creditCard.Charge());
       }
   }
}
