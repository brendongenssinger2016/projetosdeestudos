﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Unity;
using Unity.Injection;
using Unity.Lifetime;
using Unity.Resolution;

namespace UsingUnityIOC
{
    public class Program
    {
        static void Main(string[] args)
        {
         
            Aula_ControllingLifeCycle(new UnityContainer());
            Console.Read();
        }

        static void Aula_UsingTheContainer(UnityContainer unityContainer)
        {
            //  var shopper = unityContainer.Resolve<Shopper>(); 
            // var shopper = unityContainer.Resolve<Shopper>(new ParameterOverride("creditCard", new Visa()));
            var shopper = unityContainer.Resolve<Shopper>();
            shopper.Charge();
            Console.WriteLine(shopper.ChargesForCurrentCartd);
        }

        static void Aula_ControllingLifeCycle(UnityContainer unityContainer)
        {
            //var container = new UnityContainer();
            unityContainer.RegisterType<ICreditCard, MasterCard>(new ContainerControlledLifetimeManager());

            var shopper = unityContainer.Resolve<Shopper>();
            shopper.Charge();
            Console.WriteLine(shopper.ChargesForCurrentCartd);
            var shopper2 = unityContainer.Resolve<Shopper>();
            Console.WriteLine(shopper2.ChargesForCurrentCartd);
        }
    }

    
}
