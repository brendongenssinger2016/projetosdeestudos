﻿using DesenvolvimentoComCamadas.DAO;
using DesenvolvimentoComCamadas.Interface;
using DesenvolvimentoComCamadas.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesenvolvimentoComCamadas.BLL
{
    // Camada BLL ou Business, camada de lógica do sistema.
    public class CamadaBLL : ClassAbstract
    {
        private INegocios _camadaDao = null;
        public CamadaBLL(INegocios pCamadaDao)
        {
            _camadaDao = pCamadaDao;
        }
        public override void Mensagem(Type pType)
        {
            pType = typeof(CamadaBLL);
            base.Mensagem(pType);
            Console.WriteLine($"Chamando a classe {_camadaDao.GetType().Name}");
            _camadaDao.Mensagem(_camadaDao.GetType());            
        }

        public override ClienteModel AcessData()
        {
            var dados = _camadaDao.AcessData(); // Busca os dados;
            #region ValidaOsDados
            foreach (var item in dados.GetType().GetProperties())
            {
               
                if(item.PropertyType == typeof(string))
                {
                    if (string.IsNullOrEmpty(item.GetValue(dados, null).ToString()))
                        throw new Exception($"{(item.GetCustomAttributes(false)[0] as FieldsInterno).Nome} vazio.");
                }
                else if (item.PropertyType == typeof(int))
                {
                    if (string.IsNullOrEmpty(item.GetValue(dados, null).ToString()))
                        throw new Exception($"{(item.GetCustomAttributes(false)[0] as FieldsInterno).Nome} vazio.");
                }
            }
            Console.WriteLine("Validando o nome da pessoa.");
            if (string.IsNullOrEmpty(dados.Nome))
                throw new Exception("Nome Vazio.");
            Console.WriteLine("Validando o telefone");
            if (string.IsNullOrEmpty(dados.Telefone))
                throw new Exception("Telefone Vazio.");
            else
            {
                Console.WriteLine("Dados corretos");
                return dados;
            }
            #endregion


        }

        public override void Executar()
        {
            throw new NotImplementedException();
        }
    }
}
