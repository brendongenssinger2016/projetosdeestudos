﻿using DesenvolvimentoComCamadas.Interface;
using DesenvolvimentoComCamadas.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesenvolvimentoComCamadas.DAO
{
    public class CamadaDAO : ClassAbstract
    {
        //Camada que acessa a base de dados e retorna os dados.
        public override void Mensagem(Type pType)
        {
            pType = typeof(CamadaDAO);
            base.Mensagem(pType);
            Console.WriteLine($"Classe atual {typeof(CamadaDAO).Name}");
        }

        public override ClienteModel AcessData()
        {
            var clienteModel = new ClienteModel()
            {
                Id = 1,
                Nome = "Brendon Genssinger Almeida Mascarenhas",
                Email = "brendon.genssinger@gmail.com",
                Endereco = "Rua com a esquina",
                Ativo = true,
                Observacao = "Observacao",
                Telefone = "981094731"
            };
            Console.WriteLine("Dados carregados com sucesso.");
            return clienteModel;
        }

        public override void Executar()
        {
            throw new NotImplementedException();
        }
    }
}
