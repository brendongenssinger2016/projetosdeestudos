﻿using DesenvolvimentoComCamadas.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesenvolvimentoComCamadas.Interface
{
    public interface INegocios
    {
        void Executar();
        void Executar(INegocios pNegocios);
        void Mensagem(Type pType);
        ClienteModel AcessData();
    }
}
