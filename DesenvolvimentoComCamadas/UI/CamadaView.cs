﻿using DesenvolvimentoComCamadas.BLL;
using DesenvolvimentoComCamadas.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesenvolvimentoComCamadas.UI
{
    public sealed class CamadaView : ClassAbstract
    {
        private INegocios _camadaBll;
        private static readonly CamadaView _instance = new CamadaView();
        private  CamadaView()
        {
        }
        public static CamadaView Instance()
        {
            return _instance;
        }
        public override void Executar(INegocios pCamadaBll)
        {
            _camadaBll = pCamadaBll;
            Mensagem(typeof(CamadaView));
            var dados = _camadaBll.AcessData();
            Console.WriteLine("Mostrando os dados");
            Console.WriteLine($"ID :{dados.Id} \n Nome:{dados.Nome}\n Telefone:{dados.Telefone}\n Endereco:{dados.Endereco}");
        }

        public override void Mensagem(Type pType)
        {
            pType = typeof(CamadaView);
            base.Mensagem(pType);            
            Console.WriteLine($"Chamando a classe {_camadaBll.GetType().Name}");
            _camadaBll.Mensagem(null);            
            
        }

        public override void Executar()
        {
            throw new NotImplementedException();
        }
    }
}
