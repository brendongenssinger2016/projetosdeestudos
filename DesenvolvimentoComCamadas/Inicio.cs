﻿using DesenvolvimentoComCamadas.BLL;
using DesenvolvimentoComCamadas.DAO;
using DesenvolvimentoComCamadas.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesenvolvimentoComCamadas
{
    public class Inicio
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Chamando a view");
            var camadaView = CamadaView.Instance(); // using padrão Singleton
            camadaView.Executar(new CamadaBLL(new CamadaDAO())); // using padrão Builder
            Console.ReadLine();
        }
    }
}
