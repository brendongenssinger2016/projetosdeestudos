﻿using DesenvolvimentoComCamadas.Interface;
using DesenvolvimentoComCamadas.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesenvolvimentoComCamadas
{
    public abstract class ClassAbstract : INegocios
    {
        public virtual ClienteModel AcessData()
        {
            throw new NotImplementedException();
        }

        public abstract void Executar();

        public virtual void Executar(INegocios pNegocios)
        {
            throw new NotImplementedException();
        }

        public virtual void Mensagem(Type pType)
        {            
            Console.WriteLine($"Classe Atual {pType.Name}");            
        }
    }
}
