﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesenvolvimentoComCamadas.Model
{
    
    public class ClienteModel
    {
        [FieldsInterno(Nome = "ID")]
        public int Id { get; set; }
        [FieldsInterno(Nome="Nome")]
        public string Nome { get; set; }
        [FieldsInterno(Nome = "Endereco")]
        public string Endereco { get; set; }
        [FieldsInterno(Nome = "Nome")]
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Observacao { get; set; }
        public bool Ativo { get; set; }
    }
}
