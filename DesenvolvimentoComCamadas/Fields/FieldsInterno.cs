﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesenvolvimentoComCamadas
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FieldsInterno : Attribute
    {
        public FieldsInterno()
        {
        }

        public string Nome { get; set; }
    }
}
