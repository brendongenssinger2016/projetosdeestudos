﻿using System;
using System.Diagnostics;
using DicasEArmadilhas;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestUnitDicasEArmadilhas
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ExecutarPart3()
        {
            /*Uso obsoleto no metodo, ele não pode ser extendido.
             * 
             */
        }

        [TestMethod]
        public void ExecutarPart5()
        {
            Part5.NaughtyWordCensor();
            //ChamarAClasse.Chamar();
        }

        [TestMethod]
        public void ExecutarPart6()
        {
            //Part6.instancia.UsingConditionalDirectives();
            //Part6.instancia.UsingClassPathIO();
            //Part6.instancia.UseFulGeneralMethods();
            //Part6.instancia.TheEasierWay();
            //Part6.instancia.WriteDebugExample();
            //var teste = new Person("Brendon",2,"");
            //var teste1 = new Person("Teste",32);
            Part6.instancia.SystemRelatedInfo();
            Part6.instancia.DirectoryRelatedInfo();
        }
    }
}
