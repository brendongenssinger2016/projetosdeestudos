﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EvalServiceLibrary.Interfaces;
using System.ServiceModel.Description;
using System.ServiceModel;
using EvalServiceLibrary.Service;
using EvalServiceLibrary.Model;
namespace Cliente
{
    public class Cliente
    {
        static void Main(string[] args)
        {               
            Console.WriteLine("Cliente **");            
            var servidor = new EvalService();                
            var novoEval = new Eval
            {
                Comments = "Comments",
                Submitter = "Yes",
                TimeSent = DateTime.Now.Date
            };
            Console.WriteLine("Comunicando e testando.");
            servidor.Submit(novoEval);   
        }
    }
}
