﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EstudosDeEstruturaDeDados
{
    public class Program
    {
        /***
         * Função do programa é apenas trabalhar com os dados na memória, é um estudo apenas pra praticar o uso
         * de array, com um tamanho limitado de linhas 10 e 12 colunas.
         * Caso seja cadastrado algum tamanho maior do que está capacidade, o sistema dará a opção de criar um novo array,
         * de acordo com a capacidade que foi informado.
         * Outra opção foi de listar e pesquisar. 
         */

        private const int qtdDeLinhas = 10;
        private const int qtdDeColunas = 12;
        private static string[,] matriz = new string[qtdDeLinhas, qtdDeColunas];
        private static int cont,numberSaida = 1;
        static void Main(string[] args)
        {
            Console.WriteLine("BEM VINDO AO PROJETO DE ESTUDOS DE ESTRUTURA DE DADOS...");
            while(numberSaida >= 0)
            {
                CarregarDadosNaMatriz();
                Console.WriteLine("Digite a opção que deseja :\n" +
                                  "[0] Sair\n" +
                                  "[1] Pesquisar alguma Matriz \n" +
                                  "[2] Verificar o texto em alguma matriz \n" +
                                  "[4] Salvar nova Matriz\n" +
                                  "[5] Listar Matriz\n" +
                                  "[6] Excluir registro da Matriz");
                var numberDigitado = Console.ReadLine().asInt32();
                switch (numberDigitado)
                {
                    case 1: 
                        PesquisarMatriz();
                        break;
                    case 2:
                        PesquisarOTextoEmCadaMatriz();
                        break;
                    case 4:
                        Console.WriteLine("Digite o número da linha, Coluna e a numeração que deseja salvar ? ");
                        var splitLinhaEColuna = Console.ReadLine().Split(',');
                        CadastrarNovaMatriz(splitLinhaEColuna);
                        break;
                    case 0:
                        numberSaida = -1;
                        return;
                    case 5:
                        ListarMatriz();
                        break;
                    case 6:
                        ExcluirRegistroDaMatriz();
                        break;
                }
                Console.WriteLine("Deseja Sair ? Digite o valor menor que zero.");
                numberSaida = Console.ReadLine().asInt32();
            }
            Console.WriteLine("Sistema Encerrado !!");
            Console.Read();
        }

        private static void ExcluirRegistroDaMatriz()
        {
            Console.WriteLine("Informar a linha, coluna que deseja excluir.");
            var linhaEColuna = Console.ReadLine()?.Split(',');
            var linha = linhaEColuna[0].asInt32();
            var col = linhaEColuna[1].asInt32();
            matriz[linha, col] = null;
            Console.WriteLine("Dado Excluido com sucesso.");
        }

        private static void ListarMatriz()
        {
            foreach (var s in matriz)
            {
                if (s !=null)
                    Console.WriteLine(s);
            }
        }

        private static void PesquisarOTextoEmCadaMatriz()
        {
            Console.WriteLine("Digite o valor do dado que deseja encontrar");
            var numeroDoDado = Console.ReadLine().asInt32();
            var matrizEncontrado="";
            bool valorEncontrado = false;
            Console.WriteLine(matriz.GetLength(0));
            Console.WriteLine(matriz.GetLength(1));
            for (int i = 0; i < matriz.GetLength(0); i++)
            {
                for (int j = 0; j < matriz.GetLength(1); j++)
                {
                    var texto = $"Linha {i} - Coluna{j} Dados{numeroDoDado}";
                    if (texto.Equals(matriz[i, j]))
                    {
                        matrizEncontrado += $"Matriz encontrada,Linha [{i}] - Coluna[{j}]\n";
                        valorEncontrado = true;
                    }
                }
            }
            if(valorEncontrado)
                Console.WriteLine(matrizEncontrado);
                else
                    Console.WriteLine("Texto não encontrado");
        }

        public static void CarregarDadosNaMatriz()
        {
            for (int i = 0; i < qtdDeLinhas; i++)
            {
                for (int j = 0; j < qtdDeColunas; j++)
                {
                    cont++;
                    matriz[i, j] = $"Linha {i} - Coluna{j} Dados{cont}";
                }
            }
        }

        public static void PesquisarMatriz()
        {
            Console.WriteLine("Digite a numeracao da matriz pra trazer o valor\n");
            Console.WriteLine("Informa a linha e a coluna" , ConsoleColor.Red);
            var stringNumeroDaMatriz = Console.ReadLine().Split(',');
            var linha = stringNumeroDaMatriz[0];
            var coluna = stringNumeroDaMatriz[1];
            if (linha is null || coluna is null)
            {
                Console.WriteLine("Por favor informa a linha e a coluna");
                return;
            }
            var valorDaMatriz = matriz[linha.asInt32(), coluna.asInt32()];
            if (valorDaMatriz is null)
            {
                Console.WriteLine("Matriz não possui valor.");
            }
            else
            {
                Console.WriteLine("Valor Da Matriz Encontrado \n");
                Console.WriteLine(valorDaMatriz);
            }
        }

        public static void CadastrarNovaMatriz(string[] texto)
        {
            try
            {
                matriz[texto[0].asInt32(), texto[1].asInt32()] =
                    $"Linha {texto[0].asInt32()}-Coluna{texto[1].asInt32()} Dados{texto[2]}";
            }
            catch (Exception)
            {
                Console.WriteLine("Fora do indice, deve aumentar o tamanho do erro, " +
                                  "informar a quantidade de linhas, colunas.?");
                var qtd = Console.ReadLine().Split(',');
                if (qtd != null)
                { 
                    while(matriz.GetLength(0) < qtd[0].asInt32() || matriz.GetLength(0) == qtd[0].asInt32())
                    {
                        Console.WriteLine("TAMANHO DA MATRIZ INVALIDO, POR FAVOR INFORMAR NOVAMENTE.");
                        qtd = Console.ReadLine()?.Split(',');
                            if(qtd is null)
                                Console.WriteLine("MATRIZ NÃO INFORMADA, POR FAVOR TENTE NOVAMENTE.");
                    }
                }

                matriz = new string[qtd[0].asInt32(),qtd[0].asInt32()];
                CarregarDadosNaMatriz();
                Console.WriteLine("Cadastrando novos registros");
                matriz[texto[0].asInt32(), texto[1].asInt32()] = $"Linha {texto[0].asInt32()}-Coluna{texto[1].asInt32()} Dados{texto[2]}";
                Console.WriteLine($"Registro de Dados: {texto[2]}, salvo com sucesso.");
                
            }
            
        }

        public static int asInt(string matriz)
        {
            return Convert.ToInt32(matriz);
        }
        
    }
}
