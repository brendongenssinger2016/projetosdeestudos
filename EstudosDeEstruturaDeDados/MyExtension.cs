﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudosDeEstruturaDeDados
{
    public static class MyExtension
    {
        public static int asInt32(this string sender)
        {
            return Convert.ToInt32(sender);
            
        }
    }
}
