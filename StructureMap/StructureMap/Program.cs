﻿using System;
using InfraEstrutura.App;
using StructureMap;
using StructureMap.Pipeline;

namespace NameStructureMap
{
    class Program
    {
        static void Main(string[] args)
        {
           // Aula_Registering();
           Aula_OtherFeatures();
        }

        public static void Aula_Registering()
        {
            using (var container = new StructureMap.Container(x=>x.For<ICreditCard>().Use<MasterCard>().Named(("master"))))
            {
                var shopper = container.GetInstance<Shopper>();
                shopper.Charge();
                Console.WriteLine(shopper.ChargesForCurrentCartd);

                var shopper2 = container.GetInstance<Shopper>();
                shopper2.Charge();
                Console.WriteLine(shopper2.ChargesForCurrentCartd);
                Console.Read();
            }
        }

        public static void Aula_OtherFeatures()
        {
            var container = new Container(new MyRegistry());
            var shopper = container.GetInstance<Shopper>();
            shopper.Charge();
            
            Console.WriteLine(container.WhatDoIHave());
            Console.WriteLine(shopper.ChargesForCurrentCartd);
            Console.ReadKey();
        }
    }

    public class MyRegistry : Registry
    {
        public MyRegistry()
        {
            For<ICreditCard>().LifecycleIs(new SingletonLifecycle()).Use<MasterCard>();
        }
    }

    
}
