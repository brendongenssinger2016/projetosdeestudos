﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BindWPF.Model;
using System.Collections.ObjectModel;
namespace BindWPF.ViewModel
{
    public class StudentViewModel
    {
        public StudentViewModel()
        {
            LoadStudent();
        }

        public ObservableCollection<Student> Students{get;set;}
        public void LoadStudent()
        {
            ObservableCollection<Student> students = new ObservableCollection<Student>();
            students.Add(new Student { FirstName = "Brendon Genssinger Almeida", LastName = "Mascarenhas" });
            students.Add(new Student { FirstName = "Sara", LastName = "Moraes" });
            
            Students = students;
        }
    }
}
