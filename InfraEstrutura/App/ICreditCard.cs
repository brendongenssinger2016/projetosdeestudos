﻿namespace InfraEstrutura.App
{
    public interface ICreditCard
    {
        string Charge();
        int ChargeCount { get; }
    }
}
