﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfraEstrutura.App
{
   public class Shopper
   {
       private readonly ICreditCard _creditCard;

       public Shopper(ICreditCard creditCard)
       {
           this._creditCard = creditCard;
       }

       public int ChargesForCurrentCartd => _creditCard.ChargeCount;

       public void Charge()
       {
           Console.WriteLine(_creditCard.Charge());
       }
   }
}
