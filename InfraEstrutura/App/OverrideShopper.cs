﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfraEstrutura.App
{
    public class OverrideShopper
    {
        private ICreditCard creditCard { get; set; }

        public OverrideShopper(ICreditCard creditCard)
        {
            this.creditCard = creditCard;
        }

        public int ChargesForCurrentCartd
        {
            get { return creditCard.ChargeCount; }
        }
        public void Charge()
        {
            Console.WriteLine(creditCard.Charge());
        }
    }
}
