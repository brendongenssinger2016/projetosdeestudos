﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Ninject.Modules;

namespace UsingArchiveConfig.Ioc
{
    public class CadastrandoIndependencia : NinjectModule
    {
        public override void Load()
        {
            Bind<IConfig>().To<Config>().InSingletonScope();
        }
    }
}
