﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Security.Policy;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using Ninject;
using UsingArchiveConfig.Ioc;


namespace UsingArchiveConfig
{
    class Program
    {
        const string NomeDoArquivo = "Configuracoes.confi";
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel(new CadastrandoIndependencia());
            var directory = Directory.GetCurrentDirectory();
            var pathCombine = Path.Combine(directory, NomeDoArquivo);

            if (File.Exists(pathCombine))
                LerArchiveConfig(kernel, NomeDoArquivo);
            else
                CreateArchiveConfig(pathCombine);

            var dados = LerArchiveConfig(kernel, NomeDoArquivo);
            Console.WriteLine($"{dados.Database} / {dados.Password} / {dados.User} / {dados.Password1}");
        }

        private static void CreateArchiveConfig(string pCaminhoDoArquivo)
        {
            var properties = typeof(Config).GetProperties();
            var inteiro = 1;
            XElement root = null;
            XDocument doc = null;
            foreach (var propertyInfo in properties.OrderBy(px => px.Attributes))
            {
                if (inteiro == 1)
                {
                    root = new XElement(typeof(Config).Name);
                    doc = new XDocument(root);
                    inteiro = 2;
                }
                var element1 = new XElement(propertyInfo.Name);
                element1.SetAttributeValue("Value", "Vazio");
                doc?.Root?.Add(element1);
            }

            doc?.Save(pCaminhoDoArquivo);
        }

        private static IConfig LerArchiveConfig(IKernel kernel, string pNomeDoArchive)
        {
            var Xkernel = kernel.Get<IConfig>();
            
            var directory = Directory.GetCurrentDirectory();
            var pathCombine = Path.Combine(directory, pNomeDoArchive);
            var element = XElement.Load(pathCombine);
            foreach (var propertys in typeof(Config).GetProperties())
            {
                if (propertys == null) continue;
                var dados = element.Element(propertys.Name)?.FirstAttribute.Value;
                var propertyInfo = typeof(Config).GetProperty(propertys.Name);
                propertyInfo?.SetValue(Xkernel, dados);

            }
            return Xkernel;
        }
    }

    public interface IConfig
    {
        string Database { get; set; }
        string User { get; set; }
        string Password { get; set; }
        string Password1 { get; set; }

    }

    public class Teste : XmlElement
    {
        protected internal Teste(string prefix, string localName, string namespaceURI, XmlDocument doc) : base(prefix, localName, namespaceURI, doc)
        {
        }
    }

    
}
