﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expressao.Lambda
{
    class Program
    {
        static void Main(string[] args)
        {
            UsingLamba.ExecuteCsharp1();
        }
    }

    static class UsingLamba
    {
        public delegate int IncreaseByANumber(int j);
        public delegate int MultipleIncreaseByANumber(int j, int k, int l);

         public static int MultiplyByANumber(int j =10)
        {
            return j * 42;
        }

        public static void ExecuteCsharp1()
        {
            /** Primeira Forma
             */
            IncreaseByANumber increase =
                new IncreaseByANumber(UsingLamba.MultiplyByANumber);
            /** Segunda Forma
             * */
            MultipleIncreaseByANumber multiple = (j, k, l) => ((j * 42) / k) % 1;

            Console.Write(increase(10));
            Console.WriteLine(multiple(1,2,3));
            Console.Read();
        }

    }
}
