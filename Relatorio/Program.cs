﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FastReport;
using FastReport.Data;
using FastReport.Utils;

namespace Relatorio
{
    public class Program
    {
        public static ModelDados[] XDados { get; set; }

        [STAThread]
        public static void Main(string[] args)
        {
            var Report = new Report();
            var listaDeDados = new List<ModelDados>();
            listaDeDados.Add(
            new ModelDados
            {
                Nome = "Brendon",
                SobreNome = "Genssinger"
            });
            listaDeDados.Add(
            new ModelDados
            {
                Nome = "Teste1",
                SobreNome = "Teste2"
            });
            XDados = listaDeDados.ToArray();
            var watermark = new Watermark();
            watermark.Enabled = false;
            watermark.TextRotation = WatermarkTextRotation.ForwardDiagonal;
            watermark.ImageSize = WatermarkImageSize.Zoom;
            watermark.ShowTextOnTop = true;
            watermark.Text = "Visualização";
            watermark.TextFill = new HatchFill(System.Drawing.Color.Red, System.Drawing.Color.Transparent, System.Drawing.Drawing2D.HatchStyle.Percent25);
            foreach (var item in Report.Pages.OfType<ReportPage>())
            {
                item.Watermark = watermark;
            }
            using (var teste = new Report())
            {   
                try
                {
                    teste.Load("Templates/RelatorioDeSelos.frx");
                    teste.RegisterData(XDados, "Selos");                    
                    teste.Prepare(true);
                    teste.Design();
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }
        }
    }

    [DataContract]
    [KnownType(typeof(ModelDados))]
    public class ModelDados
    {
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public string SobreNome { get; set; }
    }

   



 
}
