﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using EvalServiceLibrary.Model;
namespace EvalServiceLibrary.Interfaces
{
    [ServiceContract]
    public interface IEvalService
    {
        [OperationContract]
        void Submit(Eval eval);
        [OperationContract]
        List<Eval> GetEvals();
    }
}
