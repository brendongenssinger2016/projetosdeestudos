﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using EvalServiceLibrary.Interfaces;
using EvalServiceLibrary.Model;
namespace EvalServiceLibrary.Service
{        
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)] // Uma unica instancia.
    public class EvalService : IEvalService {
        List<Eval> evals = new List<Eval>();

        public EvalService()
        {           

        }
        public void Submit(Eval eval)
        {
            Console.WriteLine($"{eval.Comments},{eval.Submitter}");
        }

        public List<Eval> GetEvals()
        {
            return evals;
        }

        public void Dispose()
        {
            this.Dispose();
        }
    }
}
