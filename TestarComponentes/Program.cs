﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Components;

namespace TestarComponentes
{
    class Program
    {
        public static void Main(string[] args)
        {
            TestarArquivoConfig te = new TestarArquivoConfig();
            Console.WriteLine(te.DataBase);
            Console.Read();
        }
    }


    public class TestarArquivoConfig : XBaseConfig
    {
        public string DataBase
        {
            get { return Get(() => DataBase); }
        }

        
    }
}
