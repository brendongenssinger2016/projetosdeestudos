﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncESync
{
    public static class Class1
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Executando async");
            Console.ForegroundColor = ConsoleColor.Black;
            Teste1(10);
            Teste2(10);
            Main(args);
            Console.Read();
        }

        public static async Task<string> Teste1(int i)
        {
            await Task.Run(() =>
            {
                for (int j = 0; j < i; j++)
                {
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("\nIniciando o teste(1)"+"Contador - "+j);
                    Console.BackgroundColor = ConsoleColor.White;
                }
            });
            return "Teste(1) OK!";
        }

        public static async Task<string> Teste2(int i)
        {
            await Task.Run(() =>
            {
                for (int j = 0; j < i; j++)
                {
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write("\n\nIniciando o teste(2)"+"Contador - "+j);
                }
            });
            return "Teste(2) OK!";
        }
    }
}
