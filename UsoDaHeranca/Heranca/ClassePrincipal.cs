﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsoDaHeranca.Heranca.Model;
namespace UsoDaHeranca.Heranca
{
    public abstract class ClassePrincipal
    {
        public List<Pessoas> listaDePessoas = new List<Pessoas>();

        public ClassePrincipal()
        {
            listaDePessoas = new List<Pessoas> {new Pessoas
            {
                Id=1,Name="Brendon"
            }};
            listaDePessoas.Add(new Pessoas
            {
                Id=2,Name="Sara"
            });
        }

        public virtual int CalcularValor()
        {
            return (int) this.listaDePessoas.Sum(px => px.Id);
        }
    }
}
