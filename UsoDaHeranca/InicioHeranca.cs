﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using UsoDaHeranca.Heranca.Model;
namespace UsoDaHeranca
{
    public class InicioHeranca
    {
        static void Execute()
        {
            Square square = new Square();
            square.Area();
        }
    }
    // Crio a classe abstrata.
    abstract class ShapesClass
    {
        public abstract int Area();
    }
    // Crio a classe normal.
    class Square : ShapesClass
    {
        private int side = 0;
        public override int Area()
        {
            return side * side;
        }
    }

    // Crio a interface;s
    interface I
    {
        void M();
    }

    // Crio a class abstrata;
    public abstract class C : I
    {
        public abstract void M();

        public void testar()
        {
            
        }

    }
    //Crio a classe filha que dependerá da classe abstrata;
    public class J : C
    {
        public override void M()
        {
        }
    }

   
}
