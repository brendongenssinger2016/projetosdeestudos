﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMapper.Iniciante.Model
{
    public class AlunoViewItem
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Endereco { get; set; }
        public string Genero { get; set; }
        public int Idade { get; set; }
        public string Nascimento { get; set; }
    }
}
