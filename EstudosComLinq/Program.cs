﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudosComLinq
{
    class Program
    {
        
        static void Main(string[] args)
        {
            GroupByEx4();

        }

        private static void GroupByEx4()
        {
            var listaDePets = 
                new List <Pet>{
                        new Pet { Name="Barley", Age=8.3 },
                       new Pet { Name="Boots", Age=4.9 },
                       new Pet { Name="Whiskers", Age=1.5 },
                       new Pet { Name="Daisy", Age=4.3 } };

            var query = listaDePets.GroupBy(pet => Math.Floor(pet.Age),
                    pet => pet.Age,
                    (baseAge, ages) => new
                    {
                        Key = baseAge,
                        Count = ages.Count(),
                        Min = ages.Min(),
                        Max = ages.Max()
                    });
            foreach (var result in query)
            {
                Console.WriteLine($"Teste {result.Key}");
                Console.WriteLine($"Teste {result.Max}");
                Console.WriteLine($"Teste {result}");
            }
        
        }
    }

    class Pet
    {
        public string Name { get; set; }
        public double Age { get; set; }
    }
}
