﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace AulasIOC.VariosTiposDeIOC
{
    public class Aula4
    {
        // Manula Dependency Injection  - Modulo 4 / Video 4

        public static void IOCContainerDemo()
        {
            //ICreditCard creditCard = new MasterCard();
            //ICreditCard otherCreditCard = new Visa();
            Resolver resolve = new Resolver();
            //var shopper = new Shopper(resolve.ResolveCreditCard());

            resolve.Register<Shopper, Shopper>();
            resolve.Register<ICreditCard,MasterCard>();
            var shopper = resolve.Resolve<Shopper>();

            shopper.Charge();
            Console.WriteLine();
            Console.Read();
            
        }
    }

    public class Resolver
    {
        //Modulo 4 - 5º Aula 
        //public ICreditCard ResolveCreditCard()
        //{
        //    if (new Random().Next(2) == 1)
        //        return new Visa();
        //    else
        //        return new MasterCard();
        //}
        
        private Dictionary<Type,Type> dependencyMap = new Dictionary<Type, Type>();
        public T Resolve<T>()
        {
            return (T)Resolve(typeof (T));
        }

        private object Resolve(Type typeToResolve)
        {
            Type resolvedType = null;
            try
            {
                resolvedType = dependencyMap[typeToResolve];
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Could not resolve Type {0}",typeToResolve.FullName ));
                throw;
            }
            var firstConstructor = resolvedType.GetConstructors().First(); // Reflexão, vai retornar todo tipo de construtor que pertence a esse.
            var constructorParamenters = firstConstructor.GetParameters();
            if (constructorParamenters.Count() == 0)
                return Activator.CreateInstance(resolvedType);
            IList<object> parameters = new List<object>();
            foreach (var parameterToResolve in constructorParamenters)
            {
                parameters.Add(Resolve(parameterToResolve.ParameterType));
            }

            return firstConstructor.Invoke(parameters.ToArray());
        }

        public void Register<TFrom, TTo>()
        {
            dependencyMap.Add(typeof(TFrom),typeof(TTo));
        }
    }
    public class Visa : ICreditCard
    {
        public string Charge()
        {
            return "Swiping the Visa !";
        }
    }

    public class MasterCard : ICreditCard
    {
        public string Charge()
        {
            return "Swiping the MasterCard!";
        }
    }

    public class Shopper
    {
        private readonly ICreditCard creditCard;

        public Shopper(ICreditCard creditCard)
        {
            this.creditCard = creditCard;
        }

        public void Charge()
        {
            var chargeMessage = creditCard.Charge();
            Console.WriteLine(chargeMessage);
        }
    }

    public interface ICreditCard
    {
        string Charge();
    }
}