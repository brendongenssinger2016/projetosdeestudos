﻿using System;
using CommunicatingBetweenControls.Model;

namespace CommunicatingBetweenControls
{
    public class EmployeChangeEventArgs : EventArgs
    {
        public Employee Employee { get; set; }
    }
}