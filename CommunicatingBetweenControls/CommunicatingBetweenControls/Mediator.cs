﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunicatingBetweenControls.Model;
namespace CommunicatingBetweenControls
{
    public sealed class Mediator
    {
        private static readonly Mediator _Instance = new Mediator();
        private Mediator(){}
        public static Mediator GetInstance()
        {
            return _Instance;
        }

        //Instance Funcionality
        public event EventHandler<JobChangedEventArgs> JobChanged;

        public event EventHandler<EmployeChangeEventArgs> EmployeChanged;
        public void OnJobChanged(object sender, Job job)
        {
            (JobChanged as EventHandler<JobChangedEventArgs>)?.Invoke(sender, new JobChangedEventArgs { Job = job }); // Carrega as pessoas.

        }

        public void OnEmployeChanged(object sender, Employee employee)
        {
            (EmployeChanged as EventHandler<EmployeChangeEventArgs>)?.Invoke(sender,new EmployeChangeEventArgs{Employee = employee});
        }


    }
}
