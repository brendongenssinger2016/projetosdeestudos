﻿using System;
using CommunicatingBetweenControls.Model;

namespace CommunicatingBetweenControls
{
    public class Mediator
    {//class unica - Designer Pattern Singletone;

        private static readonly Mediator _Instance = new Mediator();
        private Mediator() { }

        public static Mediator GetInstance()
        {
            return _Instance;
        }

        //Funcionalidade da Instance
        public event EventHandler<JobChangedEventArgs> JobChanged;

        public void OnJobChanged(object sender, Job job)
        {
            var jobChangeDelegate = JobChanged as EventHandler<JobChangedEventArgs>;
            if (jobChangeDelegate != null)
            {
                jobChangeDelegate(sender,new JobChangedEventArgs{Job = job});
            }
        }

        

    }
}