﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapperMacoratti.Net.Model;
using AutoMapperMacoratti.Net.TesteEngegregraph;

namespace AutoMapperMacoratti.Net
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                AutoMapperConfig.Teste();

                var teste = Mapper.Map<XFebrabanFileDto>(new XFebrabanFile()
                {
                    FebrabanTransacao = new Caliburn.Micro.BindableCollection<XFebrabanTransacao>()                
                }) ;
            }
            catch (Exception eX)
            {
                
            }

            //Console.WriteLine("Iniciando Mapeamento de dados");

            //var config = new MapperConfiguration(cfg => 
            //{
            //    cfg.CreateMap<Aluno, AlunoViewItem>();
            //});

            //IMapper _Imapper = config.CreateMapper();
            //var source = new Aluno();
            //source.Nome = "Brendon";
            //source.Idade = 23;
            //source.Nascimento = DateTime.Parse("13/11/1994");
            //source.Endereco = new Endereco();
            //var destination = _Imapper.Map<Aluno, AlunoViewItem>(source);
            //Console.WriteLine($"Mapper Nome 2 =>  {destination.Nome2}\n" +
            //    $"Mapper Nome => {destination.Nome}\n" +
            //    $"Mapper Nascimento => {destination.Nascimento}\n" +
            //    $"Mapper Endereco => {destination.Endereco}\n" +
            //    $"Mapper Idade => {destination.Idade}\n");
                
            //Console.ReadKey();
        }
    }
}
