﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Components.Configuracao;
using Library.Components.Interface;
using Ninject;
using Ninject.Modules;

namespace Library.Components.Ioc
{
    public class XRegistrarNegocio : NinjectModule
    {
        public override void Load()
        {
            Bind<IXConfig>().To<XConfig>().InTransientScope();
        }
    }
}
