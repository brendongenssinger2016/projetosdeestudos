﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Ninject.Parameters;
namespace Library.Components
{
    public static class XKernel
    {
        public static IKernel Instancia 
        {
            get; set;
        }

        public static T Get<T>()
        {
            return Instancia.Get<T>();
        }
    }
}
