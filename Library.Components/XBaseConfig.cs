﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Library.Components.Interface;
using Library.Components.Extension;
using Library.Components.Negocios;

namespace Library.Components
{
    public abstract class XBaseConfig : IXBaseConfig
    {
        
        public virtual string Get<TPropertie>(Action<TPropertie> missing_name)
        {
            throw new NotImplementedException();
        }

        public virtual string Get<TProperties>(Expression<Func<TProperties>> pExpressao)
        {
            var xNegocioConfig = XKernel.Get<XNegociosConfig>();
            return xNegocioConfig.GetPropertieArchiveConfig<TProperties>(pExpressao.Type.Name);
        }

        

        
    }
}
