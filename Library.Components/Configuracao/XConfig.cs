﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Components.Enumerados;
using Library.Components.Interface;

namespace Library.Components.Configuracao
{
    public class XConfig : IXConfig
    {
        private string _database;
        private string _user;
        private string _password;
        private XEnumTipoDeBase _xEnumTipoDeBase;


        public string Database
        {
            get { return _database; }
            set
            {
                if (value == _database) return;
                _database = value;
            }
        }

        public string User
        {
            get { return _user; }
            set
            {
                if (value == _user) return;
                _user = value;
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                if (value == _password) return;
                _password = value;
            }
        }

        public XEnumTipoDeBase TipoDeBaseDedados { get; set; }
        public string Password1 { get; set; }
    }

    

    
}
