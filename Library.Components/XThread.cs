﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace Library.Components
{
    public static class XThread
    {
        
        /*
         *
         * Executar processos background, priorizando.
         */
        public static void Executar(Action pAction,Action<Exception> pError)
        {
            
            ThreadPool.QueueUserWorkItem(pe =>
            {
                Exception excessao = null;
                try
                {
                    pAction();
                }
                catch (Exception e)
                {
                    throw excessao = e;
                }
            });
        }
    }


    //public class Teste
    //{
    //    public void Testar()
    //    {
    //        var tempoQueComecou = Stopwatch.StartNew();
    //        XThread.Executar(() =>
    //        {

    //            Console.WriteLine("Testando");

    //        }, pEx =>
    //        {
    //            if (pEx is null)
    //                Console.WriteLine("Não deu erro");
    //            else
    //            {
    //                Console.WriteLine("Deu Erro");
    //            }
    //        });
    //        tempoQueComecou.Stop();
    //        var tempoQueAcabou = tempoQueComecou.Elapsed.Seconds;
    //        Console.WriteLine($"Tempo Gasto {tempoQueAcabou}");
    //        tempoQueComecou.Reset();
    //        tempoQueComecou.Start();

    //        XThread.Executar(() =>
    //        {
    //            Console.WriteLine("Testando Com erro");
    //            throw new Exception("Errorr");

    //        }, pEx =>
    //        {
    //            if (pEx != null)
    //                Console.WriteLine("Sim Deu erro");
    //        });

    //        tempoQueComecou.Stop();
    //        Console.WriteLine($"Tempo Gasto {tempoQueComecou.ElapsedMilliseconds}");
    //    }
    //}
}
