﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Library.Components.Configuracao;
using Library.Components.Interface;
using System.Xml;
using Library.Components.Auxiliares;

namespace Library.Components.Negocios
{
    public class XNegociosConfig
    {
        private bool VerificaPraCriarArquivoCriarArquivo<T>()
        {
            var directory = Directory.GetCurrentDirectory();
            var caminho = Path.Combine(directory, ObjectConstantes.Instancia.GetNomeArchive);
            if (!File.Exists(caminho))
            {
                CriarArchive<T>(caminho, null);
                return true;
            }
            return false;
        }

        private void CriarArchive<T>(string pCaminho,IXConfig pConfig)
        {
            try
            {
                var properties = typeof(T).GetProperties();
                var inteiro = 1;
                XElement root = null;
                XDocument doc = null;
                foreach (var propertyInfo in properties.OrderBy(px => px.Attributes))
                {
                    if (inteiro == 1)
                    {
                        root = new XElement(typeof(T).Name);
                        doc = new XDocument(root);
                        inteiro = 2;
                    }
                    var element1 = new XElement(propertyInfo.Name);
                    element1.SetAttributeValue("Value", "Vazio");
                    doc?.Root?.Add(element1);
                }

                doc?.Save(pCaminho);
            }
            catch (Exception e)
            {
                throw new Exception("Não foi possível criar o arquivo, por favor verificar");
            }
            
        }

        private IXConfig ReadArchive(string pNomeArchive)
        {
            var instaciaConfig = XKernel.Get<IXConfig>();
            var element = GetElementXml();
            foreach (var propertys in typeof(IXConfig).GetProperties())
            {
                if (propertys == null) continue;
                var dados = element.Element(propertys.Name)?.FirstAttribute.Value;
                var propertyInfo = typeof(XConfig).GetProperty(propertys.Name);
                propertyInfo?.SetValue(instaciaConfig,dados);
            }
            return instaciaConfig;
        }

        public string GetPropertieArchiveConfig<T>(string pNomePropertie)
        {
            if (VerificaPraCriarArquivoCriarArquivo<T>())
            { 
                var element = GetElementXml();
                var dado = element?.Element(pNomePropertie)?.FirstAttribute.Value;
                if (string.IsNullOrEmpty(dado))
                    return dado;
                else
                    return string.Empty;
            }
            return string.Empty;
        }

        private XElement GetElementXml()
        {
            var directory = Directory.GetCurrentDirectory();
            var combinePath = Path.Combine(directory, ObjectConstantes.Instancia.GetNomeArchive);
            if(File.Exists(combinePath))
                return XElement.Load(combinePath);
            else
                throw new Exception("Arquivo de Configuração não existe.");
        }

        
    }
}
