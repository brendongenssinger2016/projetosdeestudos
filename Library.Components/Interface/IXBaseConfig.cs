﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Library.Components.Interface
{
    interface IXBaseConfig
    {
        string Get<TProperties>(Expression<Func<TProperties>> pExpression);
    }
}
