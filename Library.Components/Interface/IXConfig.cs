﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Components.Enumerados;

namespace Library.Components.Interface
{
    public interface IXConfig
    {
        string Database { get; set; }
        string User { get; set; }
        string Password { get; set; }
        XEnumTipoDeBase TipoDeBaseDedados { get; set; }
    }
}
