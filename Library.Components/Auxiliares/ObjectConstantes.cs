﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Components.Auxiliares
{
    public class ObjectConstantes
    {
        public static ObjectConstantes Instancia
        {
            get { return _instancia;}
            set
            {
                if(_instancia is null)
                    _instancia = new ObjectConstantes();
                else if (value == _instancia) return;
            }
            
        }

        private const string cNomeArchive = "Configuracao.config";
        private static ObjectConstantes _instancia;
        public string GetNomeArchive => cNomeArchive;
    }
}
