﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DelegateInEvents
{
    class Program
    {
        public static ModelPessoas _modelPessoas { get; set; }               

        private static void OnNameEvents(object pS, EventArgs pE)
        {
            switch (pS.GetType().Name)
            {
                case "ModelPessoas":                    
                    var listaDePropriedades = pS.GetType().GetProperties();
                    foreach (var item in listaDePropriedades)
                    {
                        Console.WriteLine("Cada Lista de Propriedades " + item.Name);
                        if(item.GetValue(pS) != null)
                            ConcatenarName(item, pS);
                    }
                    
                    break;
                default:
                    break;
            }
            
            
        }

        static void Main(string[] args)
        {
            _modelPessoas = new ModelPessoas(); // Instancio a classe;
            _modelPessoas.NameEvents += OnNameEvents; // Crio o evento
            _modelPessoas.FirstName = "brendon"; // Quando adiciono qualquer dado na minha propriedade, ocorrerá um evento.
            _modelPessoas.LastName = "Genssinger";
            Console.Read();
        }      

        static void ConcatenarName(PropertyInfo pProperty, object pObject)
        {
            Console.WriteLine(pProperty.Name + pProperty.GetValue(pObject));
            return;
        }
    }

    public class ModelPessoas : EventArgs
    {
        private  string _firstName { get; set; }
        private string _lastName { get; set; }
        public ModelPessoas()
        {
        }

        public string FirstName {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
                NameEvents(this, null);
            }
        }
        public string LastName {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
                NameEvents(this, null);
            }
        }
        public delegate void ConcatenacaoName(object pS, EventArgs pE);

        public event ConcatenacaoName NameEvents;
       
    }
}
