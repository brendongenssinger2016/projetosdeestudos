﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
namespace Thread
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ExecutarThread();
            var thread = new System.Threading.Thread(ExecutarMetodo) {Name = "Teste0"};
            Console.WriteLine("**************Thread1");
            thread.Priority = ThreadPriority.Normal;
            thread.Start();
            var thread2 = new System.Threading.Thread(ExecutarMetodo) { Name = "Teste1" };
            Console.WriteLine("**************Thread2");
            thread.Priority = ThreadPriority.Highest;
            thread2.Start();

            Console.Read();
        }

        public static void ExecutarMetodo()
        {

            for (int i = 0; i < 2000; i++)
            {
                Console.WriteLine("\r"+i);
            }
        }


        public static void ExecutarThread()
        {
            ThreadPool.QueueUserWorkItem(pS => 
            {
                try
                {
                    ExecutarInterno();
                }
                catch (Exception)
                {

                    throw;
                }
            });
        }

        private static void ExecutarInterno()
        {
            for (int i = 0; i < 50; i++)
            {
                Console.WriteLine($"Contador {i}");
            }
        }
    }
}
