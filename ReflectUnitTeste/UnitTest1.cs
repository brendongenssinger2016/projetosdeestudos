﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Aula_6_BuildYourOwnContainer;

namespace ReflectUnitTeste
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Can_Resolve_Types()
        {
            var ioc = new Container();
            ioc.For<ILogger>().Use<SqlServerLogger>();

            var logger = ioc.Resolve<ILogger>();

            Assert.AreEqual(typeof(SqlServerLogger), logger.GetType());
        }
    }
}
