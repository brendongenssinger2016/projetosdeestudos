﻿using PeopleViewer.SharedObjects;
using PersonRepository.Service.MyService;
using System.Collections.Generic;
using System.Linq;
using PersonRepository.Interface;

namespace PersonRepository.Service
{
    public class ServiceRepository : IPersonRepository
    {
        //PersonServiceClient ServiceProxy = new PersonServiceClient();

        private IPersonRepository _serviceProxy;

        public IPersonRepository ServiceProxy
        {
            get { if(_serviceProxy is null)
                    _serviceProxy = new ServiceRepository();
                        return _serviceProxy;
            }
            set => _serviceProxy = value;
        }


        public IEnumerable<Person> GetPeople()
        {
            return ServiceProxy.GetPeople();
        }

        public Person GetPerson(string lastName)
        {
            return ServiceProxy.GetPerson(lastName);
        }

        public void AddPerson(Person newPerson)
        {
            ServiceProxy.AddPerson(newPerson);
        }

        public void UpdatePerson(string lastName, Person updatedPerson)
        {
            ServiceProxy.UpdatePerson(lastName, updatedPerson);
        }

        public void DeletePerson(string lastName)
        {
            ServiceProxy.DeletePerson(lastName);
        }

        public void UpdatePerson(IEnumerable<Person> updatePeople)
        {
            throw new System.NotImplementedException();
        }

        public void UpdatePeople(IEnumerable<Person> updatedPeople)
        {
            ServiceProxy.UpdatePeople(updatedPeople.ToList());
        }
    }
}
