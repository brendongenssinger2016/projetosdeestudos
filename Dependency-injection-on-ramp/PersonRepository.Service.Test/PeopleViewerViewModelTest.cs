﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
using PeopleViewer.Presentation;
using PeopleViewer.SharedObjects;
using PersonRepository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PersonRepository.Service.Test
{
    [TestClass]
    public class PeopleViewerViewModelTest
    {
        private IPersonRepository _service = null;

        [TestInitialize]
        public void Setup()
        {
            var people = new List<Person>()
            {
                new Person(){FirstName = "Brendon",LastName = "Mascarenhas",Rating = 7,StartDate = DateTime.Parse("10/01/2000")},
                new Person(){FirstName = "Brendon2",LastName = "Mascarenhas2",Rating = 10,StartDate = DateTime.Parse("05/08/1994")}
            };
            //var repoMock = new Mock<IPersonRepository>(); // Instanciei o objecto pela interface
            //repoMock.Setup(r => r.GetPeople()).Returns(people); // retornei o objeto de pessoa.
            //repoMock.Setup(r => r.GetPerson(It.IsAny<string>()))
            //    .Returns((string n) => people.FirstOrDefault(p => p.LastName == n));
            //_service = repoMock.Object; // o _service recebe o objeto de pessoas, que possui os mesmos metodos da interface.
        }

        [TestMethod]
        public void People_OnRefreshCommand_IsPopulated()
        {
            var vm = new PeopleViewerViewModel(_service);
            vm.RefreshPeopleCommand.Execute(null);
            Assert.IsNotNull(vm.People);
            Assert.AreEqual(2, vm.People.Count());
        }

        [TestMethod]
        public void People_OnClearCommand_IsEmpty()
        {
            var vm = new PeopleViewerViewModel(_service);
            vm.RefreshPeopleCommand.Execute(null);
            Assert.AreEqual(2, vm.People.Count(), "Invalid");

            vm.ClearPeopleCommand.Execute(null);
            Assert.AreEqual(0, vm.People.Count());
        }

        [TestMethod]
        public void GetPeople_OnExecute_ReturnsPeople()
        {
            var repo = new ServiceRepository();
            //repo.ServiceProxy = _service;

            //Act
            var output = repo.GetPeople();

            Assert.IsNotNull(output);
            Assert.AreEqual(2, output.Count());
        }

        [TestMethod]
        public void GetPerson_OnExecuteWithInvalidValue_ReturnsPerson()
        {
            var repo = new ServiceRepository();
            //repo.ServiceProxy = _service;

            //Act
            var output = repo.GetPerson("SMITH");
            //Assert
            Assert.AreEqual("SMITH", output.LastName);

        }
    }
}
