﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using People.Service;
using PeopleViewer.SharedObjects;
using PersonRepository.Interface;

namespace PersonRepository.Service.Test
{
    [TestClass]
    public class UnitTest1
    {
        private IPersonService personRepository;
        [TestInitialize]
        public void Setup()
        {
            var people = new List<Person>()
            {
                new Person()
                {
                    FirstName = "Brendon",
                    LastName = "Mascarenhas",
                    Rating = 7,
                    StartDate = DateTime.Parse("10/01/2000")
                },
                new Person()
                {
                    FirstName = "Brendon2",
                    LastName = "Mascarenhas2",
                    Rating = 10,
                    StartDate = DateTime.Parse("05/08/1994")
                }
            };

            //var svcMock = new Mock<IPersonService>();
            //svcMock.Setup(r => r.GetPeople()).Returns(people);
            ////svcMock.Setup(r => r.GetPeople(It.IsAny<>()));
            //personRepository = svcMock.Object;
        }

        [TestMethod]
        public void TestMethod1()
        {
            
        }
    }
}
