﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Reflection.Estudos
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int>();
            Type listType = typeof(List<int>);
            Type[] parameterTypes = {typeof(int)};
            MethodInfo addMethod = listType.GetMethod("Add", parameterTypes);
            addMethod.Invoke(list, new object[] {7,2});
        }
    }
}
