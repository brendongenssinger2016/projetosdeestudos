﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExerciciosFeitos
{
    static class Program
    {
        static void Main(string[] args)
        {   
            ExecuteMetodos(EquacaoSegundoGrau._EquacaoSegundoGrau, IdadeMedia.MediaDeIdades,EstruturaRepetitiva._EstruturaRepetitiva);
        }
        static void ExecuteMetodos(params Action[] pLista)
        {
            foreach (var item in pLista)
            {
                item();
            }
        }
       
    }
}

    
