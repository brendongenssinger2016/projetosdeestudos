﻿using System;
using System.Globalization;
using System.Threading;

namespace ExerciciosFeitos
{
    public static class IdadeMedia
    {
        public static void MediaDeIdades()
        {
            int idade =0, soma =0, count =0;
            double media =0;
            while (idade >= 0)
            {
                idade = int.Parse(Console.ReadLine());
                if (idade >= 0)
                { 
                    soma = soma + idade;
                    count = count + 1;
                }
            }
            if (count > 0)
            { 
                media =(double) soma / count;
                Console.WriteLine(media.ToString("F2",CultureInfo.InvariantCulture));
            }
            else
                Console.WriteLine("IMPOSSIVEL CALCULAR");
            Console.ReadKey();
        }
    }
}