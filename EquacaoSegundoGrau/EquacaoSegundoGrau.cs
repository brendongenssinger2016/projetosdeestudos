﻿using System;
using System.Globalization;

namespace ExerciciosFeitos
{
    public static class EquacaoSegundoGrau
    {
        public static void _EquacaoSegundoGrau()
        {           
                double a, b, c, delta, x1, x2;
                string[] infor = Console.ReadLine().Split(' ');
                foreach (var s in infor)
                {
                    if (s is null)
                        throw new Exception("Valores vazio, por favor informar um novo valor...");
                }

                a = double.Parse(infor[0], CultureInfo.InvariantCulture);
                b = double.Parse(infor[1], CultureInfo.InvariantCulture);
                c = double.Parse(infor[2], CultureInfo.InvariantCulture);
                
                
                delta = b * b - 4 * a * c;
                if (a == 0.00 || delta == 0)
                {
                    Console.WriteLine("IMPOSSÍVEL CALCULAR.");
                }
                else
                {
                    x1 = (-b + Math.Sqrt(delta)) / (2.0 * a);
                    x2 = (-b - Math.Sqrt(delta)) / (2.0 * a);
                    Console.WriteLine("X1 = {0}", x1.ToString("F4", CultureInfo.InvariantCulture));
                    Console.WriteLine("X2 = {0}", x2.ToString("F4", CultureInfo.InvariantCulture));
                }

                Console.ReadKey();
            
        }
    }
}
