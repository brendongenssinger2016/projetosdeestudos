﻿using System;
using System.Globalization;

namespace ExerciciosFeitos
{
    public class EstruturaRepetitiva
    {
        public static void _EstruturaRepetitiva()
        {
            int n = 0;
            double x, y;

            n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                string[] vet = Console.ReadLine().Split(' ');
                x = double.Parse(vet[0], CultureInfo.InvariantCulture);
                y = double.Parse(vet[1],CultureInfo.InvariantCulture);
                if (y == 0.00)
                    Console.WriteLine("Não Existe");
                else
                {
                    Console.WriteLine((x/y).ToString("F3",CultureInfo.InvariantCulture));
                }
            }
            Console.ReadLine();
        }
        
}
}