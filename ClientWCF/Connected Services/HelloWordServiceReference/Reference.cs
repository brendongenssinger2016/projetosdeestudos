﻿//------------------------------------------------------------------------------
// <auto-generated>
//     O código foi gerado por uma ferramenta.
//     Versão de Tempo de Execução:4.0.30319.42000
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClientWCF.HelloWordServiceReference {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Name", Namespace="http://schemas.datacontract.org/2004/07/HelloWordService")]
    [System.SerializableAttribute()]
    public partial class Name : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FirstField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string LastField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string First {
            get {
                return this.FirstField;
            }
            set {
                if ((object.ReferenceEquals(this.FirstField, value) != true)) {
                    this.FirstField = value;
                    this.RaisePropertyChanged("First");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Last {
            get {
                return this.LastField;
            }
            set {
                if ((object.ReferenceEquals(this.LastField, value) != true)) {
                    this.LastField = value;
                    this.RaisePropertyChanged("Last");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="HelloWordServiceReference.IHelloWord")]
    public interface IHelloWord {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IHelloWord/SayHello", ReplyAction="http://tempuri.org/IHelloWord/SayHelloResponse")]
        string SayHello(ClientWCF.HelloWordServiceReference.Name name);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IHelloWord/SayHello", ReplyAction="http://tempuri.org/IHelloWord/SayHelloResponse")]
        System.Threading.Tasks.Task<string> SayHelloAsync(ClientWCF.HelloWordServiceReference.Name name);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IHelloWordChannel : ClientWCF.HelloWordServiceReference.IHelloWord, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class HelloWordClient : System.ServiceModel.ClientBase<ClientWCF.HelloWordServiceReference.IHelloWord>, ClientWCF.HelloWordServiceReference.IHelloWord {
        
        public HelloWordClient() {
        }
        
        public HelloWordClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public HelloWordClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public HelloWordClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public HelloWordClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string SayHello(ClientWCF.HelloWordServiceReference.Name name) {
            return base.Channel.SayHello(name);
        }
        
        public System.Threading.Tasks.Task<string> SayHelloAsync(ClientWCF.HelloWordServiceReference.Name name) {
            return base.Channel.SayHelloAsync(name);
        }
    }
}
