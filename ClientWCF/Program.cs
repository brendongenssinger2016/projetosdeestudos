﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientWCF.HelloWordServiceReference;
namespace ClientWCF
{
    class Program
    {
        static void Main(string[] args)
        {
            HelloWordClient hello = new HelloWordClient("NetTcpBinding_IHelloWord");
            Name person = new Name();
            person.First = "Brendon";
            person.Last = "Teste";
            Console.WriteLine(hello.SayHello(person));
            Console.ReadLine();
        }
    }
}
